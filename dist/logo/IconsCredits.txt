*************************************************
*						*
*		Icons Credits			*
*				       		*
*************************************************

Credits to Freepick for crown.png and crown-2.png
their website is https://www.freepik.com/

Credits to Smashicons for 170 logos in png, psd, sig
eps format.
their website is https://smashicons.com