package com.colombiapay.data_structures;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class MyHashTable<K, V> implements Map<K,V>{

    static final float DEFAULT_LOAD_FACTOR = 0.70f;
    static final int DEFAULT_INITIAL_CAPACITY = 103;

    private int size;
    private int capacity;
    private Entry<K, V>[] entries;

    public MyHashTable(int capacity) {
        this.capacity = capacity;
        this.entries = new Entry[capacity];
    }

    public MyHashTable() {
        this.capacity = DEFAULT_INITIAL_CAPACITY;
        this.entries = new Entry[capacity];
    }

    public int size() {
        return size;
    }

    public void put(K key, V value) {
        if (needToRehash())
            rehash();
        if(containsKey(key)){
            get(key).value = value;
        }else {
            Entry<K, V> newEntry = new Entry<>(key, value, null, capacity);
            if (haveCollision(newEntry))
                putFront(newEntry);
            else
                putEntry(newEntry);
        }
    }

    public void putEntry(Entry<K, V> newEntry) {
        entries[newEntry.getHash()] = newEntry;
        size++;
    }

    public void putFront(Entry<K, V> nextEntry) {
        nextEntry.next = entries[nextEntry.getHash()];
        entries[nextEntry.getHash()] = nextEntry;
        size++;
    }

    public boolean haveCollision(Entry<K, V> entry) {
        return entries[entry.getHash()] != null;
    }

    public boolean containsKey(K key){
        int dir = calculateHash(key, capacity);
        return Objects.nonNull(entries[dir]) && Objects.nonNull(entries[dir].get(key));
    }

    public void remove(K key) {
        int dir = calculateHash(key, capacity);
        Entry<K, V> aux = entries[dir];
        while (aux != null) {
            if (aux.key.equals(key) && !aux.deleted) {
                aux.deleted = true;
                size--;
                break;
            } else
                aux = aux.next;
        }
    }

    public void rehash() {
        capacity = (capacity * 2) + 1;
        Entry<K, V>[] newStructure = new Entry[capacity];
        Arrays.stream(entries).filter(Objects::nonNull).forEach(entry -> {
            int newDir = calculateHash(entry.key, capacity);
            newStructure[newDir] = entry;
        });
        entries = newStructure;
    }

    public boolean needToRehash() {
        return size >= (capacity * DEFAULT_LOAD_FACTOR);
    }

    public Entry<K,V> get(K key) {
        int dir = calculateHash(key, capacity);
        return entries[dir].get(key);
    }

    public V getValue(K key){
        return get(key).value;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (size == 0) return "null";
        Arrays.stream(entries).filter(Objects::nonNull).map(entry -> {
            StringBuilder tempStringBuilder = new StringBuilder();
            entry.forEach(innerEntry -> {
                if(!innerEntry.deleted)
                    tempStringBuilder.append(innerEntry).append("\n");
            });
            return tempStringBuilder.toString();
        }).filter(s -> !s.isBlank()).forEach(sb::append);
        return sb.toString();
    }

    public static int calculateHash(Object key, int capacity) {
        return key == null ? 0 : (key.hashCode() & (0x7fffffff)) % capacity;
    }

    public static int calculateHash(Object key) {
        return calculateHash(key, DEFAULT_INITIAL_CAPACITY);
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public void makeEmpty(){
        size = 0;
        capacity = DEFAULT_INITIAL_CAPACITY;
        entries = new Entry[capacity];
    }

    public Entry<K, V>[] getEntries() {
        return entries;
    }

    public class Entry<k, v> implements Iterable<Entry<k, v>> {
        int hash;
        k key;
        v value;
        boolean deleted;
        Entry<k, v> next;

        public Entry(k key, v value, Entry<k, v> next, int capacity) {
            this.hash = calculateHash(key, capacity);
            this.key = key;
            this.value = value;
            this.next = next;
            this.deleted = false;
        }

        public int getHash() {
            return hash;
        }

        public Entry<k, v> get(){
            return this;
        }

        public Entry<k, v> getNext() {
            return next;
        }

        public Entry<k, v> get(k key) {
            Entry<k, v> aux = this;
            while (aux != null) {
                if (aux.key.equals(key) && !aux.deleted) {
                    return aux;
                } else
                    aux = aux.next;
            }
            return null;
        }

        public v getValue() {
            return value;
        }

        public void setValue(v value) {
            this.value = value;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public final String toString() {
            return key + " = " + value;
        }

        @Override
        public Iterator<Entry<k, v>> iterator() {
            return new Iterator<>() {

                Entry<k, v> aux = new Entry<>(null, null, get(), capacity);

                @Override
                public boolean hasNext() {
                    return Objects.nonNull(aux.getNext());
                }

                @Override
                public Entry<k, v> next() {
                    if(Objects.nonNull(aux))
                        aux = aux.getNext();
                    return aux;
                }
            };
        }

    }
}
