package com.colombiapay.data_structures;

public class MyQueueList<T> extends MyLinkedList<T> implements Queue<T> {
    @Override
    public void enqueue(T element) {
        add(size(), element);
    }

    @Override
    public T dequeue() throws NullPointerException {
        emptyQueueVerification();
        T aux = get(0);
        remove(0);
        return aux;
    }

    @Override
    public T peek() throws NullPointerException {
        emptyQueueVerification();
        return get(0);
    }
}
