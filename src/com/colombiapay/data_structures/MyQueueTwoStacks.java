package com.colombiapay.data_structures;

/**
 * Para implementar una Cola usando dos Pilas, se podria hacer de la
 * siguiente manera, teniendo una Pila que reciba los elementos de entrada,
 * y otra Pila, que me sirve para voltear el orden de los datos y simular
 * el comportamiento de una Cola.
 */

public class MyQueueTwoStacks<T> implements Queue<T> {
    private Stack<T> input;
    private Stack<T> output;
    private int size;

    public MyQueueTwoStacks() {
        this.input = new MyStackArray<>();
        this.output = new MyStackArray<>();
        size = 0;
    }

    public void enqueue(T element) {
        input.push(element);
        size++;
    }

    public T dequeue() {
        emptyQueueVerification();
        T aux;
        while (!input.isEmpty()) {
            output.push(input.pop());
        }
        aux = output.pop();
        while (!output.isEmpty()) {
            input.push(output.pop());
        }
        size--;
        return aux;
    }

    @Override
    public T peek() {
        emptyQueueVerification();
        T aux;
        while (!input.isEmpty()) {
            output.push(input.pop());
        }
        aux = output.top();
        while (!output.isEmpty()) {
            input.push(output.pop());
        }
        return aux;
    }

    public boolean isEmpty() {
        return size <= 0;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        if (!isEmpty()) {
            StringBuilder s = new StringBuilder();
            Queue<T> aux = this; //TODO
            while (!aux.isEmpty()) {
                s.append("[");
                s.append(aux.dequeue());
                s.append("]\n");
            }

            return s + "\n";
        }
        return null;
    }

}