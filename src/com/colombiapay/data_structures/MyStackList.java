package com.colombiapay.data_structures;

/**
 * @author Marlon Andres Barreto Tejada
 */
public class MyStackList<T> implements Stack<T> {

    private ListNode<T> Head;
    private int size;

    public MyStackList() {
        size = 0;
        Head = null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T pop() {
        emptyStackVerification();
        T removedElement = Head.getElement();
        Head = Head.getNext();
        --size;
        return removedElement;
    }

    @Override
    public T top() {
        emptyStackVerification();
        return Head.getElement();
    }

    @Override
    public void push(T element) {
        if (size() == 0) {
            Head = new ListNode<>(element);
        } else {
            Head = new ListNode<>(element, Head);
        }
        size++;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean find(T elementToFind) {
        ListNode<T> aux = Head;
        for (int i = 0; i < size; i++) {
            if (aux.getElement().equals(elementToFind)) {
                return true;
            }
            aux = aux.getNext();
        }
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public String toString() {
        StringBuilder s = new StringBuilder();
        if (!isEmpty()) {
            s.append("[");
            ListNode<T> currentNode = Head;
            for (int i = 1; i < size; i++) {
                s.append(currentNode.getElement());
                s.append("]\n[");
                currentNode = currentNode.getNext();
            }
            s.append(currentNode.getElement());
            s.append("]");
        }
        return s + "\n";
    }

    @Override
    public void duplicate(Stack<T> stackToFill) {
        ListNode<T> aux = Head;
        Stack<T> stackAux = new MyStackList<>();
        for (int i = 0; i < size; i++) {
            stackAux.push(aux.getElement());
            aux = aux.getNext();
        }
        while (!stackAux.isEmpty()) {
            stackToFill.push(stackAux.pop());
        }
    }


}
