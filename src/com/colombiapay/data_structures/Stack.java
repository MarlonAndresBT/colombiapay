package com.colombiapay.data_structures;

import java.util.EmptyStackException;

@SuppressWarnings("unused")
public interface Stack<T> extends LinearDataStructure<T> {
    T pop();

    T top();

    void push(T element);

    boolean find(T elementToFind);

    void duplicate(Stack<T> stackToFill);

    default void emptyStackVerification() throws EmptyStackException {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
    }
}