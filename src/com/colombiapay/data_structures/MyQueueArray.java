package com.colombiapay.data_structures;

public class MyQueueArray<T> extends MyDynamicArray<T> implements Queue<T> {
    @Override
    public void enqueue(T element) {
        add(size(), element);
    }

    @Override
    public T dequeue() {
        T aux = get(0);
        remove(0);
        return aux;
    }

    @Override
    public T peek() {
        return get(0);
    }
}
