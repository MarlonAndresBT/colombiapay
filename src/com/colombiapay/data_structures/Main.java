package com.colombiapay.data_structures;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.System.nanoTime;

/**
 * In this class is the main method in which the tests
 * of each data structure that is implemented in its two
 * most relevant operations will be executed, add content
 * and search for content and then delete it content.
 * The structures used are: {@link MyDynamicArray}
 * {@link MyLinkedList}
 * {@link MyStackArray}
 * {@link MyStackList}
 *
 * @author Marlon Andres Barreto Tejada
 */
public class Main {

    public static final int NUMBER_OF_ITEMS_TO_TEST = 20;

    public static void main(String[] args) {

        /*
         * Array Tests
         */
        Thread arrayTest = new Thread(() -> {

            List<Integer> array = new MyDynamicArray<>();

            //The time begins to count after having the array full
            long Time = nanoTime();
            StringBuilder cout = new StringBuilder();
            cout.append(">> Array isEmpty:\t\t").append(array.isEmpty()).append("\t\t>> Array size:\t").append(array.size()).append("\n");

            //The array is filled with elements
            for (int i = 0; i < NUMBER_OF_ITEMS_TO_TEST; i++) {
                array.add(i);
            }
            cout.append(array.toString());
            cout.append(">> Array size after insert:\t").append(array.size());

            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> DynamicArray insert Time:\t\t\t").append(totalTime(Time)).append(" s");

            //This loop removes the data from the array
            Time = nanoTime();
            while (!array.isEmpty()) {
                try {
                    array.remove(array.size() - 1);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            cout.append(array.toString());
            cout.append(">> Array size after remove:\t").append(array.size());
            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> DynamicArray remove Time:\t\t\t").append(totalTime(Time)).append(" s");
            outFile(array.getClass().getSimpleName(), cout.toString());
        });
        /*
         * List Tests
         */
        Thread listTest = new Thread(() -> {

            List<Integer> linkedList = new MyLinkedList<>();

            //The time begins to count after having the list full
            long Time = nanoTime();
            StringBuilder cout = new StringBuilder();
            cout.append(">> List isEmpty:\t\t").append(linkedList.isEmpty()).append("\t\t>> List size:\t").append(linkedList.size()).append("\n");

            //The list is filled with elements
            for (int i = 0; i < NUMBER_OF_ITEMS_TO_TEST; i++) {
                linkedList.add(i);
            }
            cout.append(linkedList.toString());
            cout.append(">> List size after insert:\t").append(linkedList.size());

            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> LinkedList insert Time:\t\t\t\t").append(totalTime(Time)).append(" s");

            //This loop removes the data from the list
            Time = nanoTime();
            while (!linkedList.isEmpty()) {
                try {
                    linkedList.remove(0);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            cout.append(linkedList.toString());
            cout.append(">> List size after remove:\t").append(linkedList.size());
            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> LinkedList remove Time:\t\t\t").append(totalTime(Time)).append(" s");
            outFile(linkedList.getClass().getSimpleName(), cout.toString());
        });
        /*
         * Stack Tests
         */
        Thread stackArrayTest = new Thread(() -> {
            Stack<Integer> stack = new MyStackArray<>();

            //The time begins to count after having the list full
            long Time = nanoTime();
            StringBuilder cout = new StringBuilder();
            cout.append(">> Stack isEmpty:\t\t").append(stack.isEmpty()).append("\t\t>> Stack size:\t").append(stack.size()).append("\n");

            //The stack is filled with elements
            for (int i = 0; i < NUMBER_OF_ITEMS_TO_TEST; i++) {
                stack.push(i);
            }
            cout.append(stack.toString());
            cout.append(">> Stack size after insert:\t").append(stack.size());

            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> StackArray push Time:\t\t\t\t").append(totalTime(Time)).append(" s");

            //This loop removes the data from the stack
            Time = nanoTime();
            while (!stack.isEmpty()) {
                stack.pop();
            }
            cout.append(stack.toString());
            cout.append(">> Stack size after remove:\t").append(stack.size());
            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> Stack remove Time:\t\t\t").append(totalTime(Time)).append(" s");
            outFile(stack.getClass().getSimpleName(), cout.toString());


        });
        Thread stackListTest = new Thread(() -> {
            Stack<Integer> stack = new MyStackList<>();

            //The time begins to count after having the list full
            long Time = nanoTime();
            StringBuilder cout = new StringBuilder();
            cout.append(">> Stack isEmpty:\t\t").append(stack.isEmpty()).append("\t\t>> Stack size:\t").append(stack.size()).append("\n");

            //The stack is filled with elements
            for (int i = 0; i < NUMBER_OF_ITEMS_TO_TEST; i++) {
                stack.push(i);
            }
            cout.append(stack.toString());
            cout.append(">> Stack size after insert:\t").append(stack.size());

            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> StackArray push Time:\t\t\t\t").append(totalTime(Time)).append(" s");

            //This loop removes the data from the stack
            Time = nanoTime();
            while (!stack.isEmpty()) {
                stack.pop();
            }
            cout.append(stack.toString());
            cout.append(">> Stack size after remove:\t").append(stack.size());
            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> Stack remove Time:\t\t\t").append(totalTime(Time)).append(" s");
            outFile(stack.getClass().getSimpleName(), cout.toString());


        });
        Thread queueTwoStacksTest = new Thread(() -> {
            Queue<Integer> queue = new MyQueueTwoStacks<>();

            //The time begins to count after having the list full
            long Time = nanoTime();
            StringBuilder cout = new StringBuilder();
            cout.append(">> Queue isEmpty:\t\t").append(queue.isEmpty()).append("\t\t>> Queue size:\t").append(queue.size()).append("\n");

            //The stack is filled with elements
            for (int i = 0; i < NUMBER_OF_ITEMS_TO_TEST; i++) {
                queue.enqueue(i);
                System.out.println(queue.size());
            }
            cout.append(queue.toString());
            cout.append(">> Queue size after insert:\t").append(queue.size());

            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> QueueTwoStacks push Time:\t\t\t\t").append(totalTime(Time)).append(" s");

            //This loop removes the data from the stack
            Time = nanoTime();
            while (!queue.isEmpty()) {
                queue.dequeue();
            }
            cout.append(queue.toString());
            cout.append(">> Queue size after remove:\t").append(queue.size());
            //Time is stopped, to print how long it took to execute the algorithm.
            cout.append("\n>> Queue remove Time:\t\t\t").append(totalTime(Time)).append(" s");
            outFile(queue.getClass().getSimpleName(), cout.toString());
        });
        /*
         * All Tests
         */
        Thread tests = new Thread(() -> {
            listTest.start();
            arrayTest.start();
            stackArrayTest.start();
            stackListTest.start();
            queueTwoStacksTest.start();
        });

        tests.start();
    }

    /**
     * This function returns the time in seconds between the initialTime input
     * and the current time, both times being obtained with the nanoTime()
     * function of {@link java.lang.System#nanoTime()}
     *
     * @param initialTime a number extracted with {@link java.lang.System#nanoTime()},
     *                    which must be obtained immediately before starting
     *                    the algorithm to which the time is going to be measured.
     * @return returns the time that elapses between the
     * initialTime parameter and the current time.
     */
    public static double totalTime(long initialTime) {
        initialTime = nanoTime() - initialTime;
        return (double) initialTime / 1000000000;
    }

    public static void outFile(String name, String toPrint) {
        String fileName = "Test_" + name + ".txt";
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);
            BufferedWriter buffer = new BufferedWriter(fw);
            PrintWriter printer = new PrintWriter(buffer);
            printer.print(toPrint);
            printer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


}
