package com.colombiapay.data_structures;

/**
 * An object that provides a linear way of navigating a collection of generic objects.
 *
 * @param <T> the type of elements returned or stored by the {@link List}
 * @author Marlon Andres Barreto Tejada
 * @version 1.0
 */
public interface List<T> extends LinearDataStructure<T>, Iterable<T>{

    /**
     * Insert the specified Object in the List, as no index is specified, the specific implementation that you choose
     * will put the object at the beginning or at the end of the list, in order to obtain the greatest efficiency.
     *
     * @param element element to be inserted, it will have type {@link T} specified when the list is instantiated
     */
    void add(T element);

    /**
     * Inserts the specified object into the list, at the index position, as long as the index does not exceed the size
     * of the list, and if there is already an element at that index, all elements are moved one space to the right to
     * be able to perform insertion.
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted, it will have type {@link T} specified when the list is instantiated
     * @throws ArrayIndexOutOfBoundsException if the index is not between 0 and the size
     */
    void add(int index, T element) throws ArrayIndexOutOfBoundsException;

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the {@link T} type element at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is not between 0 and the size
     */
    T get(int index) throws IndexOutOfBoundsException;

    /**
     * Searches for the specified object and returns the index of the first occurrence within the entire List.
     *
     * @param element object to search
     * @return the index of the first occurrence of the element in this list at position index or later in the
     * list -1 if the element is not found.
     */
    int indexOf(T element);

    /**
     * Check if the index is included in the limits of the list.
     *
     * @param index index to check
     * @throws IndexOutOfBoundsException if the index is not between 0 and the size
     */
    void checkIndex(int index) throws IndexOutOfBoundsException;

    /**
     * Removes the element at the specified position in this list. Shifts every subsequent elements
     * to the left (subtracts one from their indices).
     *
     * @param index index of the element to remove
     * @throws IndexOutOfBoundsException if the index is not between 0 and the size
     */
    void remove(int index) throws IndexOutOfBoundsException;

    /**
     * Replace the value that is stored in the index with a new one.
     *
     * @param index      index of element to update
     * @param newElement Element {@link T} to replace the old element stored in this index
     * @throws IndexOutOfBoundsException if the index is not between 0 and the size
     */
    void update(int index, T newElement) throws IndexOutOfBoundsException;

}
