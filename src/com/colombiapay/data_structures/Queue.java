package com.colombiapay.data_structures;

public interface Queue<T> extends LinearDataStructure<T> {
    void enqueue(T element);

    T dequeue();

    T peek();

    default void emptyQueueVerification() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException();
        }
    }

}
