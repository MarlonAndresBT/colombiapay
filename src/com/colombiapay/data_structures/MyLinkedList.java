package com.colombiapay.data_structures;


import java.util.Iterator;
import java.util.Objects;

/**
 * @author Marlon Andres Barreto Tejada
 */

public class MyLinkedList<T> extends LinkedList<T> implements Iterable<T>{

    public MyLinkedList() {
        super();
    }

    public MyLinkedList(T element) {
        super(new ListNode<>(element));
    }

    public void add(T element) {
        add(size(), element);
    }

    @Override
    public void add(int index, T theElement) throws ArrayIndexOutOfBoundsException {
        if (index < 0 || index > getSize()) {
            throw new IndexOutOfBoundsException("index: " + index + " -- size: " + getSize());
        }

        if (index == 0 && getSize() == 0) {
            setHead(new ListNode<>(theElement));
            setTail(getHead());
        } else if (index == 0) {
            setHead(new ListNode<>(theElement, getHead()));
        } else if (index == getSize()) {
            ListNode<T> tmp = new ListNode<>(theElement);
            getTail().setNext(tmp);
            setTail(tmp);
        } else {
            ListNode<T> p = getHead();
            for (int i = 0; i < index - 2; i++) {
                p = p.getNext();
            }
            ListNode<T> aux = new ListNode<T>(theElement, p.getNext());
            p.setNext(aux);
            setTail(aux);
        }
        plusSize();
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) throws IndexOutOfBoundsException {
        checkIndex(index);
        ListNode<T> currentNode = getHead();
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.getNext();
        }
        return currentNode.getElement();
    }

    @Override
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    public int size() {
        return getSize();
    }

    @Override
    public int indexOf(T theElement) {
        ListNode<T> currentNode = getHead();
        int index = 0;
        while (currentNode != null && !currentNode.getElement().equals(theElement)) {
            currentNode = currentNode.getNext();
            index++;
        }
        if (currentNode == null) {
            return -1;
        } else {
            return index;
        }
    }

    @Override
    public void checkIndex(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= getSize()) {
            throw new IndexOutOfBoundsException("index: " + index + " -- size: " + getSize());
        }
    }

    @Override
    public void remove(int index) throws IndexOutOfBoundsException {
        checkIndex(index);
        if (index == 0) {
            setHead(getHead().getNext());
        } else {
            ListNode<T> aux = getHead();
            for (int i = 0; i < index - 1; i++) {
                aux = aux.getNext();
            }
            aux.setNext(aux.getNext().getNext());
        }
        minusSize();
    }

    @Override
    public void update(int index, T newElement) throws IndexOutOfBoundsException {
        checkIndex(index);
        ListNode<T> aux = getHead();
        for (int i = 0; i < index; i++) {
            aux = aux.getNext();
        }
        aux.setElement(newElement);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        if (!isEmpty()) {
            this.forEach(t -> {
                s.append("[");
                s.append(t);
                s.append("] ");
            });
        }
        return s + "\n";
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            ListNode<T> pointer = getHead();

            @Override
            public boolean hasNext() {
                return Objects.nonNull(pointer);
            }

            @Override
            public T next() {
                ListNode<T> aux = pointer;
                pointer = pointer.getNext();
                return aux.getElement();
            }
        };
    }

}
