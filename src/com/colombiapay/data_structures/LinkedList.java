package com.colombiapay.data_structures;

/**
 * An object that stores data in a linear way,
 * based on a collection of {@link ListNode} implementing
 * the methods of the {@link List} interface
 *
 * @param <T> the type of elements returned
 *            or stored by the {@link LinkedList}
 * @author Marlon Andres Barreto Tejada
 * @version 1.0
 */
public abstract class LinkedList<T> implements List<T> {
    private ListNode<T> Head;
    private int size;
    private ListNode<T> Tail;


    /**
     * Constructor of the linked list that receives the
     * {@link #Head} node, sets the {@link #Tail} as null and increases the
     * list {@link #size} from 0 to 1 if the {@link #Head} node received
     * is different from null.
     *
     * @param head a node of {@link ListNode} type that
     *             will be set as the head of the list.
     */
    public LinkedList(ListNode<T> head) {
        Head = head;
        Tail = null;
        if (Head != null) {
            size++;
        }
    }

    /**
     * Constructor of the linked list that sets
     * {@link #Head} and {@link #Tail} to null
     * and sets {@link #size} to 0;
     */
    public LinkedList() {
        size = 0;
        Head = null;
        Tail = null;
    }

    public ListNode<T> getHead() {
        return Head;
    }

    public void setHead(ListNode<T> head) {
        Head = head;
    }

    public ListNode<T> getTail() {
        return Tail;
    }

    public void setTail(ListNode<T> tail) {
        Tail = tail;
    }

    public int getSize() {
        return size;
    }

    public void minusSize() {
        size--;
    }

    public void plusSize() {
        size++;
    }

    @Override
    public T get(int index) throws IndexOutOfBoundsException {
        return null;
    }
}
