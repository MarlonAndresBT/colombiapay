package com.colombiapay.data_structures;

public interface BinaryTree<K, V> extends Map<K, V>{

    V getRootValue();

    List<? super V> toList();

    V findMin();

    V findMax();

    int height();

    boolean containsValue(V value);

    void preOrder();

    void inOrder();

    void postOrder();

    void levelOrder();

    String toString();
}
