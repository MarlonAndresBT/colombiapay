package com.colombiapay.data_structures;

public interface Map<K,V> {
    void put(K key, V value);

    void remove(K key);

    V getValue(K key);

    boolean containsKey(K key);

    boolean isEmpty();

    int size();

    void makeEmpty();

}
