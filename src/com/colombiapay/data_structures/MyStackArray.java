package com.colombiapay.data_structures;

/**
 * Para que la complejidad sea O(1), yo guardaria el valor minimo
 * en una variable, ese valor minimo lo actualizaria cada vez que hago
 * push, de manera que a medida que voy metiendo elementos en la pila
 * voy teniendo guardado el valor minimo en una variable
 */
public class MyStackArray<T> implements Stack<T> {

    private T[] array;
    private int topPosition;

    @SuppressWarnings("unchecked")
    public MyStackArray() {
        this.array = (T[]) new Object[16];
        this.topPosition = -1;
    }

    @Override
    public T pop() {
        emptyStackVerification();
        T temp = array[topPosition];
        --topPosition;
        return temp;
    }

    @Override
    public T top() {
        emptyStackVerification();
        return array[topPosition];
    }

    @Override
    public void push(T element) {
        if (this.topPosition == (2 * array.length) / 3) {
            resizeArray();
        }
        this.array[++this.topPosition] = element;
    }

    @Override
    public boolean isEmpty() {
        return topPosition < 0;
    }

    @Override
    public int size() {
        return topPosition + 1;
    }

    @Override
    public boolean find(T elementToFind) {
        for (int i = 0; i <= topPosition; i++) {
            if (array[i].equals(elementToFind)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        if (!isEmpty()) {
            for (int i = topPosition; i >= 0; i--) {
                s.append("[");
                s.append(array[i].toString());
                s.append("]\n");
            }
        }
        return s + "\n";
    }

    @Override
    public void duplicate(Stack<T> stackToFill) {
        for (int i = 0; i <= topPosition; i++) {
            stackToFill.push(array[i]);
        }
    }

    @SuppressWarnings("unchecked")
    private void resizeArray() {
        int newSize = array.length * 2;
        T[] arrayTemp = (T[]) new Object[newSize];
        System.arraycopy(this.array, 0, arrayTemp, 0, array.length);
        this.array = arrayTemp;
    }


}
