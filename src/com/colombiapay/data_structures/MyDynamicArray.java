package com.colombiapay.data_structures;

import java.util.Iterator;

/**
 * @author Marlon Andres Barreto Tejada
 */
public class MyDynamicArray<T> implements List<T> {
    private T[] array;
    private int lastPosition;

    @SuppressWarnings("unchecked")
    public MyDynamicArray() {
        this.array = (T[]) new Object[16];
        this.lastPosition = -1;
    }

    public void add(T element) {
        if (this.lastPosition == (2 * array.length) / 3) {
            resizeArray();
        }
        this.array[++this.lastPosition] = element;
    }

    @Override
    public void add(int index, T element) throws ArrayIndexOutOfBoundsException {
        checkIndex(index - 1); // Check that to insert the element in the index there is an element before it, if that
        // element before it does not exist, throw an exception.
        if (this.lastPosition == (2 * array.length) / 3) {
            resizeArray();
        }
        if (isEmpty()) {
            add(element);
        } else {
            lastPosition++;
            if (lastPosition - index >= 0)
                System.arraycopy(this.array, index, this.array, index + 1, lastPosition - index);
            this.array[index] = element;
        }
    }

    @Override
    public T get(int index) throws ArrayIndexOutOfBoundsException {
        checkIndex(index);
        return this.array[index];
    }

    public boolean isEmpty() {
        return this.lastPosition < 0;
    }

    public int size() {
        return this.lastPosition + 1;
    }

    @Override
    public int indexOf(T element) {
        if (!isEmpty()) {
            for (int i = 0; i < this.lastPosition; i++) {
                if (this.array[i].equals(element)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public void checkIndex(int index) {
        if (index < 0 || index > lastPosition) {
            throw new ArrayIndexOutOfBoundsException("index: " + index + " lastPosition: " + lastPosition);
        }
    }

    @Override
    public void remove(int index) {
        checkIndex(index);
        if (this.lastPosition - index >= 0) System.arraycopy(array, index + 1, array, index, this.lastPosition - index);
        this.lastPosition--;
    }

    @Override
    public void update(int index, T newElement) throws IndexOutOfBoundsException {
        checkIndex(index);
        array[index] = newElement;
    }


    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("");
        for (int i = 0; i < this.size(); i++) {
            s.append("[");
            s.append(this.get(i).toString());
            s.append("] \t\t");
            if ((i + 1) % 10 == 0) {
                s.append("\n");
            }
        }
        return s + "\n";
    }

    @SuppressWarnings("unchecked")
    private void resizeArray() {
        int newSize = array.length * 2;
        T[] arrayTemp = (T[]) new Object[newSize];
        System.arraycopy(this.array, 0, arrayTemp, 0, array.length);
        this.array = arrayTemp;

    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int index = 0;
            @Override
            public boolean hasNext() {
                return index <= lastPosition;
            }

            @Override
            public T next() {
                return array[index++];
            }
        };
    }
}
