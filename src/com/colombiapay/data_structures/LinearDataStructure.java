package com.colombiapay.data_structures;

/**
 * Interface that contains mandatory methods of linear Data Structures.
 *
 * @param <T> the type of elements returned or stored by the {@link LinearDataStructure}
 * @author Marlon Andres Barreto Tejada
 * @version 1.0
 */
public interface LinearDataStructure<T> {
    /**
     * Returns true if this list does not contain any elements.
     *
     * @return true if this list contains no elements and false otherwise
     */
    boolean isEmpty();

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    int size();

    /**
     * Returns a text string with all the elements contained in the list.
     *
     * @return string with all the elements contained in the list.
     */
    String toString();
}
