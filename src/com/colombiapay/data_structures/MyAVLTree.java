package com.colombiapay.data_structures;

public class MyAVLTree<T extends Comparable> {

    private AVlNode<T> root;
    private static final int balance = 1;

    public MyAVLTree() {
        root = null;
    }

    public void makeEmpty() {
        root = null;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public T search(T d) {
        T ele = search(d, root);
        return ele;
    }

    private T search(T d, AVlNode<T> r) {
        if (r == null) {
            throw new IllegalArgumentException("Number dont exist!!");
        }
        int compare = d.compareTo(r.data);
        if (compare < 0) {
            return search(d, r.left);
        } else if (compare > 0) {
            return search(d, r.right);
        } else {
            return r.data;
        }
    }

    public void insert(T x) {
        root = insert(x, root);
    }

    public void remove(T x) {
        root = remove(x, root);
        
    }

    private AVlNode<T> insert(T x, AVlNode<T> t) {
        if (t == null) {
            return new AVlNode<>(x, null, null);
        }
        int compareRes = x.compareTo(t.data);
        if (compareRes < 0) {
            t.left = insert(x, t.left);
        } else if (compareRes > 0) {
            t.right = insert(x, t.right);
        } else
            ;
        return balance(t);
    }

    private AVlNode<T> remove(T x, AVlNode<T> t) {
        if (t == null) {
            return t;
        }
        int compareRes = x.compareTo(t.data);
        if (compareRes < 0) {
            t.left = remove(x, t.left);
        } else if (compareRes > 0) {
            t.right = remove(x, t.right);
        } else if (t.left != null && t.right != null) {
            t.data = findMin(t.right).data;
            t.right = remove(t.data, t.right);
        } else {
            t = (t.left != null) ? t.left : t.right;

        }
        return balance(t);
    }

    public T findMin() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Empty tree!!");
        }
        return findMin(root).data;
    }

    public T findMax() {
        if (isEmpty()) {
            throw new IllegalArgumentException("Empty tree!!");
        }
        return findMax(root).data;
    }

    public void preOrder() {
        if (!isEmpty()) {
            preOrder(root);
        }
    }

    private void preOrder(AVlNode<T> root) {
        if (root != null) {
            System.out.println(root.data + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    public void inOrder() {
        if (!isEmpty()) {
            inOrder(root);
        }
    }

    private void inOrder(AVlNode<T> root) {
        if (root != null) {
            inOrder(root.left);
            System.out.println(root.data + " ");
            inOrder(root.right);
        }
    }

    public void postOrder() {
        if (!isEmpty()) {
            postOrder(root);
        }
    }

    private void postOrder(AVlNode<T> root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.println(root.data + " ");
        }
    }

    private AVlNode<T> findMin(AVlNode<T> t) {
        if (t == null) {
            return null;
        } else if (t.left == null) {
            return t;
        }
        return findMin(t.left);
    }

    private AVlNode<T> findMax(AVlNode<T> t) {
        if (t == null) {
            return null;
        } else if (t.right == null) {
            return t;
        }
        return findMax(t.right);
    }

    public boolean contains(T x) {
        return contains(x, root);
    }

    private boolean contains(T x, AVlNode<T> t) {
        if (t == null) {
            return false;
        }
        int compare = x.compareTo(t.data);
        if (compare < 0) {
            return contains(x, t.left);
        } else if (compare > 0) {
            return contains(x, t.right);
        } else {
            return true;
        }
    }

    private AVlNode<T> balance(AVlNode<T> t) {
        if (t == null) {
            return t;
        }
        if (height(t.left) - height(t.right) > balance) {
            if (height(t.left.left) >= height(t.left.right)) {
                t = leftRotation(t);
            } else {
                t = doubleLeftRotation(t);
            }
        } else if (height(t.right) - height(t.left) > balance) {
            if (height(t.right.right) >= height(t.right.left)) {
                t = rightRotation(t);
            } else {
                t = doubleRightRotation(t);
            }
        }
        t.height = Math.max(height(t.left), height(t.right)) + 1;
        return t;
    }

    public void checkBalance() {
        checkBalance(root);
    }

    private int checkBalance(AVlNode<T> t) {
        if (t == null) {
            return -1;
        }
        if (t != null) {
            int hleft = checkBalance(t.left);
            int hright = checkBalance(t.left);
            if (Math.abs(height(t.left) - height(t.right)) > 1 || height(t.left)
                    != hleft || height(t.left) != hleft) {
                System.out.println("Tree is not balanced!!");
            }
        }
        return height(t);
    }

    private AVlNode<T> leftRotation(AVlNode<T> t) {
        AVlNode<T> aux = t.left;
        t.left = aux.right;
        aux.right = t;
        t.height = Math.max(height(t.left), height(t.right)) + 1;
        aux.height = Math.max(height(aux.left), t.height) + 1;
        return aux;
    }

    private AVlNode<T> rightRotation(AVlNode<T> t) {
        AVlNode<T> aux = t.right;
        t.right = aux.left;
        aux.left = t;
        t.height = Math.max(height(t.left), height(t.right)) + 1;
        aux.height = Math.max(height(aux.right), t.height) + 1;
        return aux;
    }

    private AVlNode<T> doubleLeftRotation(AVlNode<T> node) {
        AVlNode aux;
        node.left = rightRotation(node.left);
        aux = leftRotation(node);
        return aux;
    }

    private AVlNode<T> doubleRightRotation(AVlNode<T> node) {
        AVlNode aux;
        node.right = leftRotation(node.right);
        aux = rightRotation(node);
        return aux;
    }

    private int height(AVlNode<T> t) {
        if (t == null) {
            return -1;
        } else {
            return 1 + Math.max(height(t.left), height(t.right));
        }
    }

    public int size() {
        return size(root);
    }

    private int size(AVlNode<T> t) {
        if (t == null) {
            return 0;
        }
        return size(t.left) + size(t.right) + 1;
    }

    class AVlNode<T> {

        T data;
        AVlNode<T> left;
        AVlNode<T> right;
        int height;

        public AVlNode(T data) {
            this(data, null, null);
        }

        public AVlNode(T theElement, AVlNode<T> left, AVlNode<T> right) {
            data = theElement;
            left = left;
            right = right;
            height = 0;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public AVlNode<T> getLeft() {
            return left;
        }

        public void setLeft(AVlNode<T> left) {
            this.left = left;
        }

        public AVlNode<T> getRight() {
            return right;
        }

        public void setRight(AVlNode<T> right) {
            this.right = right;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

    }

}
