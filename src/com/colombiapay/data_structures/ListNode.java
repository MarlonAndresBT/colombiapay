package com.colombiapay.data_structures;

/**
 * @author Marlon Andres Barreto Tejada
 */
public class ListNode<T> {

    private ListNode<T> previous;
    private T element;
    private ListNode<T> next;

    public ListNode() {
        this.previous = null;
        this.element = null;
        this.next = null;
    }

    public ListNode(T element) {
        this.previous = null;
        this.element = element;
        this.next = null;
    }

    public ListNode(T element, ListNode<T> next) {
        this.element = element;
        this.next = next;
    }

    public ListNode(ListNode<T> previous, T element) {
        this.previous = previous;
        this.element = element;
    }

    public ListNode<T> getPrevious() {
        return previous;
    }

    public void setPrevious(ListNode<T> previous) {
        this.previous = previous;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public ListNode<T> getNext() {
        return next;
    }

    public void setNext(ListNode<T> next) {
        this.next = next;
    }
}
