package com.colombiapay;

import com.colombiapay.data_structures.MyStackArray;
import com.colombiapay.data_structures.Stack;
import com.colombiapay.ui.ColombiaPayLogin;
import com.colombiapay.ui.styles.ColombiaPanel;
import com.colombiapay.view.ColombiaPay;

import javax.swing.*;
import java.awt.*;

public class App extends JFrame {

    public static final Dimension WINDOW_SIZE = new Dimension(1395, 1057);
    private static App mainWindow;
    private final Stack<ColombiaPanel> backNavigator;

    private App() {
        setTitle("ColombiaPay");
        backNavigator = new MyStackArray<>();
        setLayout(new CardLayout());
        add(ColombiaPayLogin.getInstance());
    }

    public static synchronized App getInstance() {
        if (mainWindow == null)
            mainWindow = new App();
        return mainWindow;
    }

    public Stack<ColombiaPanel> getBackNavigator() {
        return backNavigator;
    }

    public void setPanel(ColombiaPanel currentPanel, ColombiaPanel newPanel) {
        setContentPane(newPanel);
        backNavigator.push(currentPanel);
    }

    public void goBack() {
        boolean canGoBack = !backNavigator.isEmpty();
        if (canGoBack)
            setContentPane(backNavigator.pop());
    }
    
    public static void main(String[] args) {
        ColombiaPay.initializeApp();
        App window = App.getInstance();
        window.setBounds(0, 0, 1395, 1057);
        window.setVisible(true);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}