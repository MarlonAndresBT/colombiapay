package com.colombiapay.view;

import com.colombiapay.data_structures.BinaryTree;
import com.colombiapay.data_structures.MyBinaryTree;
import com.colombiapay.data_structures.MyHashTable;
import com.colombiapay.model.ErrorLog;
import com.colombiapay.model.exceptions.UserNotFoundException;
import com.colombiapay.model.factories.ClientFactory;
import com.colombiapay.model.factories.PaymentMethodFactory;
import com.colombiapay.model.persistence.ClientFileStore;
import com.colombiapay.model.persistence.PaymentMethodFileStore;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.model.user_data.User;

import javax.security.sasl.AuthenticationException;
import java.io.File;
import java.io.FileNotFoundException;

public class ColombiaPay {

    private static ColombiaPay colombiaPay;
    public boolean initializedApp = false;
    private MyHashTable<Long, Client> clients;

    private ColombiaPay() {
        this.clients = new MyHashTable<>();
    }

    public void deleteUser(User user, String password) throws AuthenticationException, UserNotFoundException {
        if (clients.containsKey(user.getCC())) {
            Long userID = user.getCC();
            Client clientToDelete = clients.getValue(userID);
            clientToDelete.checkPassword(password);
            clients.remove(userID);
        }
        throw new UserNotFoundException();
    }


    public MyHashTable<Long, Client> getClients() {
        return clients;
    }

    public void deleteClient(Long cc) throws UserNotFoundException {
        if (clients.containsKey(cc))
            clients.remove(cc);
        throw new UserNotFoundException();
    }

    public void addClient(Client user) {
        clients.put(user.getCC(), user);
    }

    public Client getUser(Long cc) throws UserNotFoundException {
        if (clients.containsKey(cc))
            return clients.getValue(cc);
        throw new UserNotFoundException(cc.toString());
    }

    public static synchronized ColombiaPay getInstance() {
        if (colombiaPay == null)
            colombiaPay = new ColombiaPay();
        return colombiaPay;

    }

    public static void initializeApp() {
        ColombiaPay app = ColombiaPay.getInstance();
        try {
            app.clients = ClientFactory.build(new File(ClientFileStore.FILE_PATH));
            PaymentMethodFactory.build(new File(PaymentMethodFileStore.FILE_PATH));
            app.initializedApp = true;
        } catch (FileNotFoundException e) {
            ErrorLog.report(e);
        }
    }

    public static void finalizeApp() {
        System.out.println("Finalizando ColombiaPay...");
        ColombiaPay app = ColombiaPay.getInstance();
        ClientFileStore.saveData(app.clients);
        System.out.println("App finalizada correctamente");
    }

    public static void main(String[] args) {

        //ConsoleMenu.welcomeLogo();
        //ConsoleMenu.startMenu();

        /*ColombiaPay app = getInstance();
        initializeApp();
        System.out.println("Number of clients registered in ColombiaPay: " + app.clients.size());
        System.out.println(app.clients.isEmpty());
        System.out.println(app.clients.get(1000134309L).toString());
        System.out.println(app.clients.get(1657093093499L).toString());
        System.out.println(app.clients.get(1682052451199L).toString());
        System.out.println(app.clients.get(1000134309L).toString());
        //System.out.println("Numbers of payment methods of our users: " + PaymentMethodFactory.sizeOfLastQueue);
        finalizeApp();*/

        /*Client client = app.clients.get(45);
        String namePaymentM = client.getPaymentMethods().get(0).getClass().getName();
        System.out.println(namePaymentM);
        Card card = (Card)client.getPaymentMethods().get(0);
        int pass = card.getCVC();
        String passS = String.valueOf(pass);

        Calendar date = Calendar.getInstance();

        PublicService luz = new PublicService(2.0," ",date,"luz");

        client.addPayable(luz);

        Payable aPagar = client.getPayables().get(0);
        client.pay(card,aPagar,passS);*/
    }

}
