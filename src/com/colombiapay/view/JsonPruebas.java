package com.colombiapay.view;

import com.colombiapay.model.exceptions.InvalidClientDataException;
import com.colombiapay.model.factories.ClientFactory;
import com.colombiapay.model.payment_methods.CreditCard;
import com.colombiapay.model.persistence.FileStore;
import com.colombiapay.model.user_data.Client;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public class JsonPruebas {
    public static void main(String[] args) {

        try {
            Client client = ClientFactory.build("1661011144199 Quail Elijah Delacruz Chaney 31 12 1969 U_Kent@colombiapay.com 1129984787 TtU57P@#$sQ9PM");

            Reader myReader = Files.newBufferedReader(Path.of(FileStore.DATA_PATH + "creditcard_data/" + client.getName() + ".json"));
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            CreditCard c = gson.fromJson(myReader, CreditCard.class);

            System.out.println(c.toString());


            //Instancie todos los metodos de pago en una cola, los separe en 3 colas segun el tipo (Credito, Debito, ColombiaCredits)
            //Luego guarde el datao de cada metodo de pago de cada persona en un archivo json aparte nombrado con el nolmbre de su dueño
            // y guardado en una carpeta nombrada segun el tipo de metodo de pago.
            /*Queue<PaymentMethod> listaPrueba = PaymentMethodFactory.buildForTest(new File(FileStore.DATA_PATH + "PaymentMethodData.txt"));
            Queue<CreditCard> creditCardQueue = new MyQueueList<>();
            Queue<DebitCard> debitCardQueue = new MyQueueList<>();
            Queue<ColombiaCredits> colombiaCreditsQueue = new MyQueueList<>();
            while (!listaPrueba.isEmpty()) {
                if (listaPrueba.peek().getClass().getSimpleName().equals("DebitCard")) {
                    debitCardQueue.enqueue((DebitCard) listaPrueba.dequeue());
                }else if (listaPrueba.peek().getClass().getSimpleName().equals("CreditCard")) {
                    creditCardQueue.enqueue((CreditCard) listaPrueba.dequeue());
                }else if (listaPrueba.peek().getClass().getSimpleName().equals("ColombiaCredits")) {
                    colombiaCreditsQueue.enqueue((ColombiaCredits) listaPrueba.dequeue());
                }
            }
            System.out.println("ColombiaCredits size: " + debitCardQueue.size());
            while(!colombiaCreditsQueue.isEmpty()){
                String fileName = FileStore.DATA_PATH + "colombiacredits_data/" + colombiaCreditsQueue.peek().getOwnerName() + ".json";
                try(PrintWriter pw = new PrintWriter(new File(fileName))){
                    pw.write(gson.toJson(colombiaCreditsQueue.dequeue()));
                    pw.flush();
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }*/

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidClientDataException e) {
            e.printStackTrace();
        }
    }
}
