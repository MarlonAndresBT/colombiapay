package com.colombiapay.view;

import com.colombiapay.model.exceptions.InvalidClientDataException;
import com.colombiapay.model.exceptions.UserNotFoundException;
import com.colombiapay.model.factories.ClientFactory;

import java.nio.DoubleBuffer;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.DoubleAdder;

public interface ConsoleMenu {

    Scanner input = new Scanner(System.in);

    static void startMenu() {
        while (true) {
            int option;
            System.out.println("1. Start server");
            System.out.println("2. End program");
            System.out.print(">>> ");
            String inputAux = input.next();
            try {
                option = validateOption(inputAux);
            } catch (IllegalArgumentException e) {
                option = 0;
            }
            switch (option) {
                case 1 -> {
                    System.out.println("Starting server...");
                    startServer();
                }
                case 2 -> endProgram();
                default -> System.out.println("Insert a valid option");
            }
        }

    }

    static void startServer() {
        while (true) {
            System.out.println("1. Load program data automatically");
            System.out.println("2. Manipulate data manually");
            System.out.println("3. Get the number of Users");
            System.out.println("4. Count the money that users have in the App");
            System.out.println("5. Save program data");
            System.out.println("6. Back");
            int option;
            System.out.print(">>> ");
            String inputAux = input.next();
            try {
                option = validateOption(inputAux);
            } catch (IllegalArgumentException e) {
                option = 0;
            }
            switch (option) {
                case 1 -> loadData();
                case 2 -> createData();
                case 3 -> countUsers();
                case 4 -> countMoney();
                case 5 -> saveData();
                case 6 -> startMenu();
                default -> System.out.println("Insert a valid option");
            }
        }
    }

    static void saveData() {
        ColombiaPay.finalizeApp();
    }

    static void countMoney() {
        DoubleAdder acc = new DoubleAdder();
        Arrays.stream(ColombiaPay.getInstance().getClients().getEntries()).filter(Objects::nonNull).forEach(entry -> {
                        entry.getValue().getPaymentMethods().forEach(paymentMethod -> {
                            boolean isColombiaCredits = paymentMethod.getClass().getSimpleName().equals("ColombiaCredits");
                            acc.add(isColombiaCredits ? paymentMethod.countMoney() : 0);
                        });
        });
        System.out.println("Users have a total of "
                + NumberFormat.getCurrencyInstance().format(acc)
                + " COP in their ColombiaCredits wallet");
    }

    static void countUsers() {
        System.out.println("There are " + ColombiaPay.getInstance().getClients().size() + " registered in the App");
    }

    static void createData() {
        while (true) {
            System.out.println("1. Create user");
            System.out.println("2. Insert payment method");
            System.out.println("3. Insert payable");
            System.out.println("4. Delete user");
            System.out.println("8. Save program data");
            System.out.println("9. Back");
            int option;
            System.out.print(">>> ");
            String inputAux = input.next();
            try {
                option = validateOption(inputAux);
            } catch (IllegalArgumentException e) {
                option = 0;
            }
            switch (option) {
                case 1 -> createUser();
                case 2 -> insertPaymentMethod();
                case 3 -> insertPayable();
                case 4 -> deleteUser();
                case 8 -> saveData();
                case 9 -> startServer();
                default -> System.out.println("Insert a valid option");
            }
        }
    }

    static void deleteUser() {
        System.out.println("██    ██ ███████ ███████ ██████                          \n" +
                "██    ██ ██      ██      ██   ██                         \n" +
                "██    ██ ███████ █████   ██████                          \n" +
                "██    ██      ██ ██      ██   ██                         \n" +
                " ██████  ███████ ███████ ██   ██                         \n" +
                "                                                         \n" +
                "                                                         \n" +
                "██████  ███████ ██      ███████ ████████ ███████ ██████  \n" +
                "██   ██ ██      ██      ██         ██    ██      ██   ██ \n" +
                "██   ██ █████   ██      █████      ██    █████   ██████  \n" +
                "██   ██ ██      ██      ██         ██    ██      ██   ██ \n" +
                "██████  ███████ ███████ ███████    ██    ███████ ██   ██ \n" +
                "                                                         \n" +
                "                                                         ");
        System.out.print("Please insert the identification number of the user to delete.\n>>> ");
        Long cc = input.nextLong();
        System.out.println("Are you sure?");
        System.out.println("1. Yes");
        System.out.println("2. No");
        int option;
        System.out.print(">>> ");
        String inputAux = input.next();
        try {
            option = validateOption(inputAux);
        } catch (IllegalArgumentException e) {
            option = 0;
        }
        switch (option) {
            case 1 -> {
                try {
                    ColombiaPay.getInstance().deleteClient(cc);
                } catch (UserNotFoundException e) {
                    System.out.println(e.getMessage());
                }
            }
            case 2 -> {
                System.out.println("No user was deleted, going back ...");
            }
            default -> System.out.println("Insert a valid option");
        }
    }

    static void insertPayable() {
        System.out.println("██████   █████ ██    ██  █████  ██████  ██      ███████ \n" +
                "██   ██ ██   ██ ██  ██  ██   ██ ██   ██ ██      ██      \n" +
                "██████  ███████  ████   ███████ ██████  ██      █████   \n" +
                "██      ██   ██   ██    ██   ██ ██   ██ ██      ██      \n" +
                "██      ██   ██   ██    ██   ██ ██████  ███████ ███████ \n" +
                "                                                        \n" +
                "                                                        ");

        System.out.println("To insert a payable, remember to enter the data as follows.");
        System.out.println("PublicService Amount Reference DD MM YYYY");
        System.out.println("ElectronicService Amount Reference DD MM YYYY");
        System.out.println("TelecommunicationsService Amount Reference DD MM YYYY");
        System.out.println("Tax Amount Reference DD MM YYYY");
        System.out.println("Credit Amount DD MM Dues Payed");
        System.out.println("Transfer Amount DestinationAccount");
        System.out.println();
        System.out.println("1. Continue");
        System.out.println("2. Back");
        int option;
        System.out.print(">>> ");
        String inputAux = input.next();
        try {
            option = validateOption(inputAux);
        } catch (IllegalArgumentException e) {
            option = 0;
        }
        switch (option) {
            case 1 -> {
                System.out.print(">>> ");
                inputAux = input.nextLine();
                System.out.println("");
            }
            case 2 -> createData();
            default -> System.out.println("Insert a valid option");
        }
    }

    static void insertPaymentMethod() {
        System.out.println("██████   █████ ██    ██ ███    ███ ███████ ███    ██ ████████ \n" +
                "██   ██ ██   ██ ██  ██  ████  ████ ██      ████   ██    ██    \n" +
                "██████  ███████  ████   ██ ████ ██ █████   ██ ██  ██    ██    \n" +
                "██      ██   ██   ██    ██  ██  ██ ██      ██  ██ ██    ██    \n" +
                "██      ██   ██   ██    ██      ██ ███████ ██   ████    ██    \n" +
                "                                                              \n" +
                "                                                              \n" +
                "███    ███ ███████ ████████ ██   ██  ██████  ██████           \n" +
                "████  ████ ██         ██    ██   ██ ██    ██ ██   ██          \n" +
                "██ ████ ██ █████      ██    ███████ ██    ██ ██   ██          \n" +
                "██  ██  ██ ██         ██    ██   ██ ██    ██ ██   ██          \n" +
                "██      ██ ███████    ██    ██   ██  ██████  ██████           \n" +
                "                                                              \n" +
                "                                                              ");

        System.out.println("To insert a payment method, remember to enter the data as follows.");
        System.out.println("CreditCard **** **** **** **** MM YYYY CVC Balance Amount Name Name Surname Surname\n" +
                "DebitCard **** **** **** **** MM YYYY CVC Balance Name Name Surname Surname\n" +
                "ColombiaCredits CellPhone Balance Name Name Surname Surname");
        System.out.println("1. Continue");
        System.out.println("2. Back");
        int option;
        System.out.print(">>> ");
        String inputAux = input.next();
        try {
            option = validateOption(inputAux);
        } catch (IllegalArgumentException e) {
            option = 0;
        }
        switch (option) {
            case 1 -> {
                System.out.print(">>> ");
                inputAux = input.nextLine();
                try {
                    ColombiaPay.getInstance().addClient(ClientFactory.build(inputAux));
                    System.out.println("User created successfully");
                } catch (InvalidClientDataException e) {
                    System.out.println("Incorrect input data, user not created");
                }
            }
            case 2 -> createData();
            default -> System.out.println("Insert a valid option");
        }
    }

    static void createUser() {
        System.out.println("██    ██ ███████ ███████ ██████                          \n" +
                "██    ██ ██      ██      ██   ██                         \n" +
                "██    ██ ███████ █████   ██████                          \n" +
                "██    ██      ██ ██      ██   ██                         \n" +
                " ██████  ███████ ███████ ██   ██                         \n" +
                "                                                         \n" +
                "                                                         \n" +
                " ██████ ██████  ███████  █████ ████████  ██████  ██████  \n" +
                "██      ██   ██ ██      ██   ██   ██    ██    ██ ██   ██ \n" +
                "██      ██████  █████   ███████   ██    ██    ██ ██████  \n" +
                "██      ██   ██ ██      ██   ██   ██    ██    ██ ██   ██ \n" +
                " ██████ ██   ██ ███████ ██   ██   ██     ██████  ██   ██ \n" +
                "                                                         \n" +
                "                                                         ");

        System.out.println("To create a user, remember that you must insert the data in the following order.");
        System.out.println("Identification number Name DD MM YYYY Email Cellphone Password");
        System.out.println("1. Continue");
        System.out.println("2. Back");
        int option;
        System.out.print(">>> ");
        String inputAux = input.next();
        try {
            option = validateOption(inputAux);
        } catch (IllegalArgumentException e) {
            option = 0;
        }
        switch (option) {
            case 1 -> {
                StringBuilder client = new StringBuilder();
                System.out.print(">>> ");
                Scanner aux = new Scanner(System.in);
                client.append(aux.nextLine());
                try {
                    ColombiaPay.getInstance().addClient(ClientFactory.build(client.toString()));
                    System.out.println("User created successfully");
                } catch (InvalidClientDataException e) {
                    System.out.println("Incorrect input data, user not created");
                }
            }
            case 2 -> createData();
            default -> System.out.println("Insert a valid option");
        }
    }

    static void loadData() {
        System.out.println("Loading data...");
        ColombiaPay.initializeApp();
        System.out.println("Data loaded");
    }

    static int validateOption(String input) {
        if (input.matches("^\\d*$")) {
            return Integer.parseInt(input);
        }
        throw new IllegalArgumentException();
    }

    static void endProgram() {
        input.close();
        System.exit(0);
    }

    static void welcomeLogo() {
        System.out.println("██     ██ ███████ ██      ██████  ██████  ███    ███ ███████                           \n" +
                "██     ██ ██      ██     ██      ██    ██ ████  ████ ██                                \n" +
                "██  █  ██ █████   ██     ██      ██    ██ ██ ████ ██ █████                             \n" +
                "██ ███ ██ ██      ██     ██      ██    ██ ██  ██  ██ ██                                \n" +
                " ███ ███  ███████ ███████ ██████  ██████  ██      ██ ███████                           \n" +
                "                                                                                       \n" +
                "                                                                                       \n" +
                "████████  ██████                                                                       \n" +
                "   ██    ██    ██                                                                      \n" +
                "   ██    ██    ██                                                                      \n" +
                "   ██    ██    ██                                                                      \n" +
                "   ██     ██████                                                                       \n" +
                "                                                                                       \n" +
                "                                                                                       \n" +
                " ██████  ██████  ██      ██████  ███    ███ ██████  ██  █████  ██████   █████ ██    ██ \n" +
                "██      ██    ██ ██     ██    ██ ████  ████ ██   ██ ██ ██   ██ ██   ██ ██   ██ ██  ██  \n" +
                "██      ██    ██ ██     ██    ██ ██ ████ ██ ██████  ██ ███████ ██████  ███████  ████   \n" +
                "██      ██    ██ ██     ██    ██ ██  ██  ██ ██   ██ ██ ██   ██ ██      ██   ██   ██    \n" +
                " ██████  ██████  ███████ ██████  ██      ██ ██████  ██ ██   ██ ██      ██   ██   ██    ");
    }

    static void clear() {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception ignored) {

        }
    }

}
