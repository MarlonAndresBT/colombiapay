package com.colombiapay.model.user_data;

import com.colombiapay.model.payables.Payable;
import com.colombiapay.model.payment_methods.PaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;
import java.util.Calendar;

public class Invoice {
    private Integer id;
    private PaymentMethod billedTo;
    private Payable paidTo;
    private Calendar date;

    public Invoice(Integer id, PaymentMethod billedTo, Payable paidTo, Calendar date) {
        this.id = id;
        this.billedTo = billedTo;
        this.paidTo = paidTo;
        this.date = date;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public PaymentMethod getBilledTo() {
        return billedTo;
    }

    public void setBilledTo(PaymentMethod billedTo) {
        this.billedTo = billedTo;
    }

    public Payable getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(Payable paidTo) {
        this.paidTo = paidTo;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", billedTo=" + billedTo +
                ", paidTo=" + paidTo +
                ", date=" + date +
                '}';
    }
}
