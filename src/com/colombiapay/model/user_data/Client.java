package com.colombiapay.model.user_data;

import com.colombiapay.data_structures.List;
import com.colombiapay.data_structures.MyHashTable;
import com.colombiapay.data_structures.MyLinkedList;
import com.colombiapay.model.exceptions.NotEnoughMoneyException;
import com.colombiapay.model.exceptions.NotFoundException;
import com.colombiapay.model.exceptions.cannotBeRemovedException;
import com.colombiapay.model.payables.Payable;
import com.colombiapay.model.payment_methods.ColombiaCredits;
import com.colombiapay.model.payment_methods.PaymentMethod;

import javax.naming.AuthenticationException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.atomic.DoubleAdder;

public class Client extends User {

    private List<PaymentMethod> paymentMethods;
    private List<Payable> payables;
    private List<AutoPayment> autoPayments;
    private MyHashTable<Integer, Invoice> invoices;

    public Client(Long cc, String name, Calendar birthDate, String email, Long cellPhone, String password) {
        super(cc, name, birthDate, email, cellPhone, password);
        this.payables = new MyLinkedList<>();
        this.autoPayments = new MyLinkedList<>();
        this.invoices = new MyHashTable<>();
        this.paymentMethods = new MyLinkedList<>();
    }

    public void pay(PaymentMethod billTo, Payable toPay, String password) {
        try {
            billTo.discount(toPay.amount, password);
            int id = new Random().nextInt();
            invoices.put(id, new Invoice(id,billTo, toPay, new GregorianCalendar()));
        } catch (NotEnoughMoneyException e) {
            System.out.println("No había suficiente dinero en tu cartera");
        } catch (AuthenticationException e) {
            System.out.println("Clave incorrecta");
        }
    }

    public void autoPay(AutoPayment autoPayment) {
        autoPayment.dailyCheckUp();
    }

    public void receiveMoney(Double amount) {
        for (int i = 0; i < paymentMethods.size(); i++)
            if (paymentMethods.get(i).getClass().equals(ColombiaCredits.class))
                paymentMethods.get(i).topUp(amount);
    }

    public void addPaymentMethod(PaymentMethod paymentMethod) {
        paymentMethods.add(paymentMethod);
    }

    public void addPayable(Payable payable) {
        payables.add(payable);
    }

    public void addInvoice(Invoice invoice) {
        invoices.put(invoice.getID(),invoice);
    }

    public void removePaymentMethod(String id) throws NotFoundException, cannotBeRemovedException {
        boolean found = false;
        for (int i = 0; i < paymentMethods.size(); i++)
            if (paymentMethods.get(i).getID().equals(id)) {
                if (paymentMethods.get(i).getClass().equals(ColombiaCredits.class))
                    throw new cannotBeRemovedException("By having a ColombiaPay account you automatically have a ColombiaCredits wallet that cannot be deleted.");
                paymentMethods.remove(i);
                found = true;
            }
        if (!found)
            throw new NotFoundException(id);
    }

    public void removePayable(String reference) throws NotFoundException {
        boolean found = false;
        for (int i = 0; i < payables.size(); i++)
            if (payables.get(i).getPaymentReference().equals(reference)) {
                payables.remove(i);
                found = true;
            }
        if (!found)
            throw new NotFoundException(reference);
    }

    public List<PaymentMethod> getPaymentMethods() {
        return this.paymentMethods;
    }

    public List<Payable> getPayables() {
        return payables;
    }

    public List<AutoPayment> getAutoPayments() {
        return autoPayments;
    }

    public MyHashTable<Integer,Invoice> getInvoices() {
        return invoices;
    }

    @Override
    public String toString() {
        StringBuilder clientData = new StringBuilder();
        clientData.append(cc).append(" ")
                .append(name).append(" ");
        String auxDay = "0", auxMonth = "0";
        if (birthDate.get(Calendar.DAY_OF_MONTH) < 10) auxDay += birthDate.get(Calendar.DAY_OF_MONTH);
        else auxDay = String.valueOf(birthDate.get(Calendar.DAY_OF_MONTH));
        if (birthDate.get(Calendar.MONTH) + 1 < 10) auxMonth += birthDate.get(Calendar.MONTH) + 1;
        else auxMonth = String.valueOf(birthDate.get(Calendar.MONTH) + 1);
        clientData.append(auxDay).append(" ")
                .append(auxMonth).append(" ")
                .append(birthDate.get(Calendar.YEAR)).append(" ")
                .append(email).append(" ").append(cellPhone).append(" ").append(getPassword());
        return clientData.toString();
    }

    public Double amountMoney() {
        DoubleAdder acc = new DoubleAdder();
        paymentMethods.forEach(paymentMethod -> acc.add(paymentMethod.countMoney()));
        return acc.doubleValue();
    }
}
