package com.colombiapay.model.user_data;

import com.colombiapay.model.exceptions.NotEnoughMoneyException;
import com.colombiapay.model.payables.Payable;
import com.colombiapay.model.payment_methods.PaymentMethod;

import java.util.Calendar;

public class AutoPayment {
    private PaymentMethod paymentMethod;
    private Payable toPay;
    private Calendar dueDate;

    public void dailyCheckUp() {
        Calendar today = Calendar.getInstance();
        if (dueDate.compareTo(today) >= 0) {
            try {
                paymentMethod.discount(toPay.amount);
            } catch (NotEnoughMoneyException e) {
                System.out.println("No había suficiente dinero en la cartera");
            }
        }
    }

}
