package com.colombiapay.model.user_data;

import com.colombiapay.model.exceptions.UserNotFoundException;
import com.colombiapay.view.ColombiaPay;

import javax.security.sasl.AuthenticationException;
import java.util.Calendar;

public abstract class User {
    protected Long cc;
    protected String name;
    protected Calendar birthDate;
    protected String email;
    protected Long cellPhone;
    private String password;
    private ColombiaPay colombiaPay;

    public User(Long cc, String name, Calendar birthDate, String email, Long cellPhone, String password) {
        this.cc = cc;
        this.name = name;
        this.birthDate = birthDate;
        this.email = email;
        this.cellPhone = cellPhone;
        this.password = password;
        this.colombiaPay = ColombiaPay.getInstance();
    }

    public void changeEmail(String password, String newEmail) throws AuthenticationException {
        checkPassword(password);
        this.email = newEmail;
    }

    public void changePassword(String password, String newPassword) throws AuthenticationException {
        checkPassword(password);
        this.password = newPassword;
    }

    public Boolean checkCC(Long cc) {
        return this.cc.equals(cc);
    }

    public void checkPassword(String password) throws AuthenticationException {
        if (!this.password.equals(password))
            throw new AuthenticationException("Wrong password");
    }

    public void deleteUser(String password) throws AuthenticationException {
        checkPassword(password);
        try {
            this.colombiaPay.deleteUser(this, password);
            System.out.println("User deleted");
        } catch (UserNotFoundException e) {
            System.out.println("Error, the user could not be removed.");
        }
    }

    public String getName() {
        return this.name;
    }

    protected String getPassword() {
        return password;
    }

    public Long getCC() {
        return cc;
    }

    public void setCC(Long cc) {
        this.cc = cc;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(Long cellPhone) {
        this.cellPhone = cellPhone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ColombiaPay getColombiaPay() {
        return colombiaPay;
    }

    public void setColombiaPay(ColombiaPay colombiaPay) {
        this.colombiaPay = colombiaPay;
    }
}
