package com.colombiapay.model.user_data;

import java.util.Calendar;

public class Admin extends User {

    public Admin(Long cc, String name, Calendar birthDate, String email, Long cellPhone, String password) {
        super(cc, name, birthDate, email, cellPhone, password);
    }
}
