package com.colombiapay.model;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Stream;

public class ErrorLog {

    public static final String PATH = "data/ErrorLog.txt";

    public static void report(Exception exception) {
        report(exception.getMessage(), exception.getClass().getSimpleName(), Arrays.stream(exception.getStackTrace()));
    }

    private static void report(String message, String name, Stream stackTrace) {
        StringBuilder toReport = new StringBuilder();
        if (existsFile()) {
            FileReader fileReader;
            try {
                fileReader = new FileReader(PATH);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                while (bufferedReader.ready()) {
                    toReport.append(bufferedReader.readLine()).append("\n");
                }
                bufferedReader.close();
                fileReader.close();
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        toReport.append("[")
                .append(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now()))
                .append("]\t[").append(name).append("]\t[")
                .append(message).append("]\t[");
        StringBuffer sb = new StringBuffer();
        stackTrace.forEach(stackTraceElement -> sb.append(stackTraceElement).append("\t"));
        toReport.append(sb.append("]\n").toString());
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(PATH);
            BufferedWriter buffer = new BufferedWriter(fileWriter);
            PrintWriter printer = new PrintWriter(buffer);
            printer.print(toReport);
            printer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static Boolean existsFile() {
        return new File(PATH).exists();
    }

    public static void resetLog() {
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(PATH);
            BufferedWriter buffer = new BufferedWriter(fileWriter);
            PrintWriter printer = new PrintWriter(buffer);
            printer.print("");
            printer.close();
            fileWriter.close();
        } catch (IOException ex) {
            report(ex);
        }
    }

}
