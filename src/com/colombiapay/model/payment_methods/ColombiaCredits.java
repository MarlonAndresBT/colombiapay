package com.colombiapay.model.payment_methods;

import com.colombiapay.model.exceptions.NotEnoughMoneyException;

import javax.naming.AuthenticationException;

public class ColombiaCredits implements PaymentMethod {

    private String id;
    private Double balance;
    private long ownerID;

    public ColombiaCredits(String id, Double balance, long ownerID) {
        this.id = id;
        this.balance = balance;
        this.ownerID = ownerID;
    }

    @Override
    public void discount(Double amount, String password) throws NotEnoughMoneyException, AuthenticationException {
        if (canPay(amount))
            this.balance -= amount;
        else
            throw new NotEnoughMoneyException();
    }

    @Override
    public void discount(Double amount) throws NotEnoughMoneyException {
        if (canPay(amount))
            this.balance -= amount;
        else
            throw new NotEnoughMoneyException();
    }

    @Override
    public void topUp(Double amount) {
        this.balance += amount;
    }

    @Override
    public Double countMoney() {
        return balance;
    }

    @Override
    public Boolean canPay(Double amount) {
        return balance >= amount;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public String toString() {
        StringBuilder cardData = new StringBuilder();
        cardData.append(this.getClass().getSimpleName()).append(" ")
                .append(this.id).append(" ")
                .append(this.balance).append(" ")
        ;//.append(this.owner);
        return cardData.toString();
    }

    @Override
    public long getOwnerID() {
        return ownerID;
    }
}
