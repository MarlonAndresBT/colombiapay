package com.colombiapay.model.payment_methods;

import com.colombiapay.model.exceptions.NotEnoughMoneyException;

import javax.naming.AuthenticationException;

public interface PaymentMethod {
    void discount(Double amount, String password) throws NotEnoughMoneyException, AuthenticationException;

    void discount(Double amount) throws NotEnoughMoneyException;

    void topUp(Double amount);

    Double countMoney();

    Boolean canPay(Double amount);

    String getID();

    long getOwnerID();
}
