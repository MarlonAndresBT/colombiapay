package com.colombiapay.model.payment_methods;

import java.util.Calendar;

public class CreditCard extends Card {

    private Double creditCardLimit;

    public CreditCard(String cardNumber, Calendar expirationDate, Integer cvc, long ownerID, Double balance, Double creditCardLimit) {
        super(cardNumber, expirationDate, cvc, ownerID, balance);
        this.creditCardLimit = creditCardLimit;
    }

    @Override
    public String toString() {
        StringBuilder cardData = new StringBuilder();
        cardData.append(this.getClass().getSimpleName()).append(" ")
                .append(this.getCardNumber()).append(" ")
                .append(this.expirationDate.get(Calendar.MONTH) + 1).append(" ")
                .append(this.expirationDate.get(Calendar.YEAR)).append(" ")
                .append(this.getCVC()).append(" ")
                .append(this.balance).append(" ")
                .append(this.creditCardLimit).append(" ")
        ;//.append(this.owner);
        return cardData.toString();
    }
}
