package com.colombiapay.model.payment_methods;

import com.colombiapay.model.exceptions.NotEnoughMoneyException;

import javax.naming.AuthenticationException;
import java.util.Calendar;

public abstract class Card implements PaymentMethod {

    protected String cardNumber;
    protected Calendar expirationDate;
    private Integer cvc;
    protected long ownerID;
    protected Double balance;

    public Card(String cardNumber, Calendar expirationDate, Integer cvc, long ownerID, Double balance) {
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cvc = cvc;
        this.ownerID = ownerID;
        this.balance = balance;
    }

    protected Integer getCVC() {
        return this.cvc;
    }//protected

    public Boolean checkCVC(Integer cvc) {
        return this.cvc.equals(cvc);
    }

    @Override
    public void discount(Double amount, String cvc) throws NotEnoughMoneyException, AuthenticationException {
        if (checkCVC(Integer.parseInt(cvc))) {
            if (canPay(amount)) {
                this.balance -= amount;
            } else {
                throw new NotEnoughMoneyException();
            }
        } else {
            throw new AuthenticationException("Wrong cvc");
        }
    }

    @Override
    public void discount(Double amount) throws NotEnoughMoneyException {
        if (canPay(amount)) {
            this.balance -= amount;
        } else {
            throw new NotEnoughMoneyException();
        }
    }

    @Override
    public void topUp(Double amount) {
        this.balance += amount;
    }

    @Override
    public Double countMoney() {
        return this.balance;
    }

    @Override
    public Boolean canPay(Double amount) {
        return this.balance >= amount;
    }

    @Override
    public String getID() {
        StringBuffer auxCardNumber = new StringBuffer("**** **** **** ");
        auxCardNumber.append(cardNumber.substring(cardNumber.length() - 5));
        return auxCardNumber.toString();
    }

    public long getOwnerID() {
        return ownerID;
    }

    protected String getCardNumber() {
        return this.cardNumber;
    }

}
