package com.colombiapay.model.payment_methods;

import java.util.Calendar;

public class DebitCard extends Card {

    private Integer password;
    private Double amount;

    public DebitCard(String cardNumber, Calendar expirationDate, Integer cvc, long ownerID, Double balance) {
        super(cardNumber, expirationDate, cvc, ownerID, balance);
    }

    @Override
    public String toString() {
        StringBuilder cardData = new StringBuilder();
        cardData.append(this.getClass().getSimpleName()).append(" ")
                .append(this.getCardNumber()).append(" ")
                .append(this.expirationDate.get(Calendar.MONTH) + 1).append(" ")
                .append(this.expirationDate.get(Calendar.YEAR)).append(" ")
                .append(this.getCVC()).append(" ")
                .append(this.balance).append(" ")
        ;//.append(this.owner);
        return cardData.toString();
    }
}
