package com.colombiapay.model.persistence;

import com.colombiapay.data_structures.List;
import com.colombiapay.data_structures.MyHashTable;
import com.colombiapay.model.user_data.Client;

import java.util.Arrays;
import java.util.Objects;

public interface ClientFileStore extends FileStore {

    String FILE_PATH = DATA_PATH + "ClientData.txt";

    static void saveData(MyHashTable<Long, Client> clients) {
        String[] data = ClientFileStore.extractData(clients);
        FileStore.writeData(data[0], ClientFileStore.FILE_PATH);
        FileStore.writeData(data[1], PaymentMethodFileStore.FILE_PATH);
    }

    private static String[] extractData(MyHashTable<Long, Client> clients) {
        String[] data = new String[5];
        StringBuilder clientsDataToSave = new StringBuilder(),
                paymentMethodsDataToSave = new StringBuilder(),
                payablesDataToSave = new StringBuilder(),
                autoPaymentsDataToSave = new StringBuilder(),
                invoicesDataToSave = new StringBuilder();
        Arrays.stream(clients.getEntries()).filter(Objects::nonNull).forEach(entry -> {
            entry.forEach(innerEntry -> {
                if(!innerEntry.isDeleted()) {
                    clientsDataToSave.append(innerEntry.getValue().toString()).append("\n");
                    paymentMethodsDataToSave.append(PaymentMethodFileStore.extractData(innerEntry.getValue()));
                    payablesDataToSave.append(PayablesFileStore.extractData(innerEntry.getValue()));
                    autoPaymentsDataToSave.append(" ");
                    invoicesDataToSave.append(" ");
                }
                if(innerEntry.isDeleted())
                    System.out.println(innerEntry.getValue().toString());
            });
        });
        data[0] = clientsDataToSave.toString();
        data[1] = paymentMethodsDataToSave.toString();
        data[2] = payablesDataToSave.toString();
        data[3] = autoPaymentsDataToSave.toString();
        data[4] = invoicesDataToSave.toString();
        return data;
    }

    static void saveDataFromList(List<Client> clients) {
        String[] data = ClientFileStore.extractDataFromList(clients);
        FileStore.writeData(data[0], ClientFileStore.FILE_PATH);
        FileStore.writeData(data[1], PaymentMethodFileStore.FILE_PATH);
    }

    private static String[] extractDataFromList(List<Client> clients) {
        String[] data = new String[5];
        StringBuilder clientsDataToSave = new StringBuilder(),
                paymentMethodsDataToSave = new StringBuilder(),
                payablesDataToSave = new StringBuilder(),
                autoPaymentsDataToSave = new StringBuilder(),
                invoicesDataToSave = new StringBuilder();
        for (int i = 0; i < clients.size(); i++) {
            clientsDataToSave.append(clients.get(i).toString()).append("\n");
            paymentMethodsDataToSave.append(PaymentMethodFileStore.extractData(clients.get(i)));
            payablesDataToSave.append(PayablesFileStore.extractData(clients.get(i)));
            autoPaymentsDataToSave.append(" ");
            invoicesDataToSave.append(" ");
        }
        data[0] = clientsDataToSave.toString();
        data[1] = paymentMethodsDataToSave.toString();
        data[2] = payablesDataToSave.toString();
        data[3] = autoPaymentsDataToSave.toString();
        data[4] = invoicesDataToSave.toString();
        return data;
    }

}
