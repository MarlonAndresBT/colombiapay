package com.colombiapay.model.persistence;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public interface FileStore {

    String DATA_PATH = "data/";

    static void writeData(String data, String path) {
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(path);
            BufferedWriter buffer = new BufferedWriter(fileWriter);
            PrintWriter printer = new PrintWriter(buffer);
            printer.print(data);
            printer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
