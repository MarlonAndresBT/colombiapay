package com.colombiapay.model.persistence;

import com.colombiapay.data_structures.List;
import com.colombiapay.model.payables.Payable;
import com.colombiapay.model.user_data.Client;

public interface PayablesFileStore extends FileStore {

    String FILE_PATH = DATA_PATH + "PayableData.txt";

    static String extractData(Client client) {
        StringBuilder toSave = new StringBuilder();
        List<Payable> payables = client.getPayables();
        for (int i = 0; i < payables.size(); i++) {
            toSave.append(payables.get(i).toString()).append("\n");
        }
        return toSave.toString();
    }
}
