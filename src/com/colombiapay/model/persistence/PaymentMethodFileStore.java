package com.colombiapay.model.persistence;

import com.colombiapay.data_structures.List;
import com.colombiapay.model.payment_methods.PaymentMethod;
import com.colombiapay.model.user_data.Client;

public interface PaymentMethodFileStore extends FileStore {

    String FILE_PATH = DATA_PATH + "PaymentMethodData.txt";

    static String extractData(Client client) {
        StringBuilder toSave = new StringBuilder();
        List<PaymentMethod> paymentMethods = client.getPaymentMethods();
        for (int i = 0; i < paymentMethods.size(); i++) {
            toSave.append(paymentMethods.get(i).toString()).append(client.getCC()).append("\n");
        }
        return toSave.toString();
    }
}
