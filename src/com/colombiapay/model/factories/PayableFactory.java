package com.colombiapay.model.factories;

import com.colombiapay.data_structures.MyBinaryTree;
import com.colombiapay.model.ErrorLog;
import com.colombiapay.model.exceptions.InvalidPayableDataException;
import com.colombiapay.model.payables.Credit;
import com.colombiapay.model.payables.Payable;
import com.colombiapay.model.payables.Service;
import com.colombiapay.model.payables.Transfer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public abstract class PayableFactory {
    private static final String CREDIT_REGEX = "^(Credit)\\s\\d+\\.?\\d+\\s[\\w-]+(\\s\\d{1,2}){2}\\s\\d{4}(\\s\\d+\\.\\d+){2}$";
    private static final String TRANSFER_REGEX = "^(Transfer)\\s\\d+\\.?\\d+\\s[\\w-]+(\\s\\d{1,2}){2}\\s\\d{4}\\s[\\w- _.]*$";
    private static final String SERVICE_REGEX = "^[A-Z][a-z]+\\s\\d+\\.?\\d+\\s[\\w-]+(\\s\\d{1,2}){2}\\s\\d{4}[\\w\\s]+$";
    private static final String PAYABLE_REGEX = "^[a-zA-Z]+\\s\\d+\\.?\\d+\\s[\\w-]+(\\s\\d{1,2}){2}\\s\\d{4}$";
    public static final int CREDIT = 0, TRANSFER = 1, SERVICE = 2, PAYABLE = 3;

    public static MyBinaryTree<String, Payable> build(File file) throws FileNotFoundException {
        MyBinaryTree<String, Payable> payables = new MyBinaryTree<>();
        Scanner input = new Scanner(file);
        while (input.hasNext()) {
            String payableData = input.nextLine();
            try {
                Payable payableToAdd = build(payableData);
                String paymentReference = payableToAdd.getPaymentReference();
                payables.put(paymentReference, payableToAdd);
            } catch (InvalidPayableDataException e) {
                ErrorLog.report(e);
            }
        }
        input.close();
        return payables;
    }

    public static Payable build(String payableData) throws InvalidPayableDataException {
        int typePayable = dataValidation(payableData);
        return switch (typePayable) {
            case CREDIT -> creditBuilder(payableData);
            case TRANSFER -> transferBuilder(payableData);
            case SERVICE -> serviceBuilder(payableData);
            case PAYABLE -> payableBuilder(payableData);
            default -> null;
        };
    }


    private static Credit creditBuilder(String payableData) {
        String data[] = payableData.split("\\s");
        double  amount  = Double.parseDouble(data[1]),
                dues    = Double.parseDouble(data[6]),
                payed   = Double.parseDouble(data[7]);
        int     day     = Integer.parseInt(data[3]),
                month   = Integer.parseInt(data[4]),
                year    = Integer.parseInt(data[5]);
        String reference = data[2];
        Calendar dueDate = new GregorianCalendar(year, month - 1, day);
        return new Credit(amount, reference, dueDate, dues, payed);
    }

    private static Transfer transferBuilder(String payableData) {
        String data[] = payableData.split("\\s");
        double  amount  = Double.parseDouble(data[1]);
        int     day     = Integer.parseInt(data[3]),
                month   = Integer.parseInt(data[4]),
                year    = Integer.parseInt(data[5]);
        String reference = data[2],
                destinationAccount = data[6];
        Calendar dueDate = new GregorianCalendar(year, month - 1, day);
        return new Transfer(amount, reference, dueDate, destinationAccount);
    }
    //TODO
    private static Service serviceBuilder(String payableData) {
        return null;
    }

    private static Payable payableBuilder(String payableData) {
        return null;
    }


    private static int dataValidation(String payableData) throws InvalidPayableDataException {
        if (payableData.matches(CREDIT_REGEX))
            return CREDIT;
        else if (payableData.matches(TRANSFER_REGEX))
            return TRANSFER;
        else if (payableData.matches(SERVICE_REGEX))
            return SERVICE;
        else if (payableData.matches(PAYABLE_REGEX))
            return PAYABLE;
        else
            throw new InvalidPayableDataException(payableData);
    }

}
