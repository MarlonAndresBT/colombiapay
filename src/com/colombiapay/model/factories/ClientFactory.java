package com.colombiapay.model.factories;

import com.colombiapay.data_structures.MyBinaryTree;
import com.colombiapay.data_structures.MyHashTable;
import com.colombiapay.model.ErrorLog;
import com.colombiapay.model.exceptions.InvalidClientDataException;
import com.colombiapay.model.user_data.Client;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.regex.Pattern;

public abstract class ClientFactory {

    private static final String CLIENT_REGEX = "^\\d+\\s([A-Z][a-z]*\\s?){1,6}([0-9]{2}\\s?){2}[0-9]{4}\\s\\w+[@]\\w+\\.\\w{1,3}\\s[0-9]+\\s(?=.{8,20}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).*$";

    public static MyHashTable<Long, Client> build(File file) throws FileNotFoundException {
        MyHashTable<Long, Client> clients = new MyHashTable<>();
        Scanner input = new Scanner(file);
        while (input.hasNext()) {
            String clientData = input.nextLine();
            try {
                Client clientToAdd = build(clientData);
                Long clientID = clientToAdd.getCC();
                clients.put(clientID, clientToAdd);
            } catch (InvalidClientDataException e) {
                ErrorLog.report(e);
            }
        }
        input.close();
        return clients;
    }

    public static Client build(String clientData) throws InvalidClientDataException {
        dataValidation(clientData);
        String[] data = clientData.split(" ");
        String cc = data[0],
                day = data[data.length - 6], month = data[data.length - 5],
                year = data[data.length - 4], mail = data[data.length - 3],
                cell = data[data.length - 2], pass = data[data.length - 1];
        StringBuilder name = new StringBuilder();
        for (int i = 1; i < data.length - 6; i++)
            name.append(data[i]).append(" ");
        name.deleteCharAt(name.length() - 1);
        return new Client(Long.parseLong(cc), name.toString(), new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day)), mail, Long.parseLong(cell), pass);
    }

    private static void dataValidation(String clientData) throws InvalidClientDataException {
        if (!clientData.matches(CLIENT_REGEX))
            throw new InvalidClientDataException();
    }
}
