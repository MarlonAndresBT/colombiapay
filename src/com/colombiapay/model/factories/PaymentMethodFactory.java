package com.colombiapay.model.factories;

import com.colombiapay.data_structures.BinaryTree;
import com.colombiapay.data_structures.MyHashTable;
import com.colombiapay.data_structures.MyQueueList;
import com.colombiapay.data_structures.Queue;
import com.colombiapay.model.ErrorLog;
import com.colombiapay.model.exceptions.InvalidPaymentMethodDataException;
import com.colombiapay.model.payment_methods.ColombiaCredits;
import com.colombiapay.model.payment_methods.CreditCard;
import com.colombiapay.model.payment_methods.DebitCard;
import com.colombiapay.model.payment_methods.PaymentMethod;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.view.ColombiaPay;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public abstract class PaymentMethodFactory {

    public static final int CREDIT_CARD = 1;
    public static final int DEBIT_CARD = 2;
    public static final int COLOMBIA_CREDITS = 3;
    public static int sizeOfLastQueue = 0;
    private static final String CREDIT_CARD_REGEX =
            "^(CreditCard)\\s([0-9]{4}\\s){4}([0-9]{1,2}\\s[0-9]{4}\\s)([0-9]{3}\\s)([0-9]+\\.?[0-9]*\\s){2}[0-9]+$";
    private static final String DEBIT_CARD_REGEX =
            "^(DebitCard)\\s([0-9]{4}\\s){4}([0-9]{1,2}\\s[0-9]{4}\\s)([0-9]{3}\\s)([0-9]+\\.?[0-9]*\\s)[0-9]+$";
    private static final String COLOMBIA_CREDITS_REGEX =
            "^(ColombiaCredits)\\s([0-9]+\\s)([0-9]+\\.?[0-9]*\\s)[0-9]+$";

    public static void build(File file) throws FileNotFoundException {
        Queue<PaymentMethod> paymentMethods = paymentMethodsStream(file);
        assignToUsers(paymentMethods);
    }

    private static void assignToUsers(Queue<PaymentMethod> paymentMethods) {
        MyHashTable<Long, Client> clients = ColombiaPay.getInstance().getClients();
        while (!paymentMethods.isEmpty()) {
            long clientID = paymentMethods.peek().getOwnerID();
            if (clients.containsKey(clientID)) {
                clients.getValue(clientID).addPaymentMethod(paymentMethods.dequeue());
            } else paymentMethods.dequeue();
        }
    }

    private static Queue<PaymentMethod> paymentMethodsStream(File file) throws FileNotFoundException {
        Queue<PaymentMethod> paymentMethods = new MyQueueList<>();
        Scanner input = new Scanner(file);
        while (input.hasNext()) {
            String paymentMethodData = input.nextLine();
            try {
                paymentMethods.enqueue(build(paymentMethodData));
            } catch (InvalidPaymentMethodDataException e) {
                ErrorLog.report(e);
            }
        }
        sizeOfLastQueue = paymentMethods.size();
        return paymentMethods;
    }

    public static PaymentMethod build(String paymentMethodData) throws InvalidPaymentMethodDataException {
        int typeMethod = dataValidation(paymentMethodData);
        return switch (typeMethod) {
            case CREDIT_CARD -> creditCardBuilder(paymentMethodData);
            case DEBIT_CARD -> debitCardBuilder(paymentMethodData);
            case COLOMBIA_CREDITS -> colombiaCreditsBuilder(paymentMethodData);
            default -> null;
        };
    }

    private static CreditCard creditCardBuilder(String paymentMethodData) {
        String[] aux = paymentMethodData.split("\\s");
        String cardNumber = aux[1] + " " + aux[2] + " " + aux[3] + " " + aux[4];
        Calendar expirationDate = new GregorianCalendar(Integer.parseInt(aux[6]), Integer.parseInt(aux[5]) - 1, 1);
        Integer cvc = Integer.parseInt(aux[7]);
        StringBuilder owner = new StringBuilder();
        Double balance = Double.parseDouble(aux[8]);
        Double creditCardLimit = Double.parseDouble(aux[9]);
        for (int i = 10; i < aux.length; i++)
            owner.append(aux[i]).append(" ");
        owner.deleteCharAt(owner.length() - 1);
        return new CreditCard(cardNumber, expirationDate, cvc, Long.parseLong(owner.toString()), balance, creditCardLimit);
    }

    private static DebitCard debitCardBuilder(String paymentMethodData) {
        String[] aux = paymentMethodData.split("\\s");
        String cardNumber = aux[1] + " " + aux[2] + " " + aux[3] + " " + aux[4];
        Calendar expirationDate = new GregorianCalendar(Integer.parseInt(aux[6]), Integer.parseInt(aux[5]) - 1, 1);
        Integer cvc = Integer.parseInt(aux[7]);
        StringBuilder owner = new StringBuilder();
        Double balance = Double.parseDouble(aux[8]);
        for (int i = 9; i < aux.length; i++)
            owner.append(aux[i]).append(" ");
        owner.deleteCharAt(owner.length() - 1);
        return new DebitCard(cardNumber, expirationDate, cvc, Long.parseLong(owner.toString()), balance);
    }

    private static ColombiaCredits colombiaCreditsBuilder(String paymentMethodData) {
        String[] aux = paymentMethodData.split("\\s");
        String id = aux[1];
        StringBuilder owner = new StringBuilder();
        Double balance = Double.parseDouble(aux[2]);
        for (int i = 3; i < aux.length; i++)
            owner.append(aux[i]).append(" ");
        owner.deleteCharAt(owner.length() - 1);
        return new ColombiaCredits(id, balance, Long.parseLong(owner.toString()));
    }

    private static int dataValidation(String paymentMethodData) throws InvalidPaymentMethodDataException {
        if (paymentMethodData.matches(CREDIT_CARD_REGEX))
            return CREDIT_CARD;
        else if (paymentMethodData.matches(DEBIT_CARD_REGEX))
            return DEBIT_CARD;
        else if (paymentMethodData.matches(COLOMBIA_CREDITS_REGEX))
            return COLOMBIA_CREDITS;
        else
            throw new InvalidPaymentMethodDataException(paymentMethodData);
    }
}
