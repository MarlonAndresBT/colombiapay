package com.colombiapay.model.payables;

import java.util.Calendar;

public class Tax extends Service {

    public Tax(Double amount, String paymentReference, Calendar dueDate, String name) {
        super(amount, paymentReference, dueDate, name);
    }
}
