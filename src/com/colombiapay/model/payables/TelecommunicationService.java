package com.colombiapay.model.payables;

import java.util.Calendar;

public class TelecommunicationService extends Service {

    public TelecommunicationService(Double amount, String paymentReference, Calendar dueDate, String name) {
        super(amount, paymentReference, dueDate, name);
    }
}
