package com.colombiapay.model.payables;

import java.util.Calendar;

public abstract class Service extends Payable {
    protected String name;

    Service(Double amount, String paymentReference, Calendar dueDate, String name) {
        super(amount, paymentReference, dueDate);
        this.name = name;
    }

}
