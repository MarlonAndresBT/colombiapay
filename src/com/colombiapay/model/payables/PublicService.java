package com.colombiapay.model.payables;

import java.util.Calendar;

public class PublicService extends Service {

    public PublicService(Double amount, String paymentReference, Calendar dueDate, String name) {
        super(amount, paymentReference, dueDate, name);
    }
}
