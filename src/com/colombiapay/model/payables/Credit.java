package com.colombiapay.model.payables;

import java.util.Calendar;

public class Credit extends Payable {

    private double dues;
    private double payed;

    public Credit(double amount, String paymentReference, Calendar dueDate, double dues, double payed) {
        super(amount, paymentReference, dueDate);
        this.dues = dues;
        this.payed = payed;
    }

    public double getDues() {
        return dues;
    }

    public void setDues(double dues) {
        this.dues = dues;
    }

    public double getPayed() {
        return payed;
    }

    public void setPayed(double payed) {
        this.payed = payed;
    }
}
