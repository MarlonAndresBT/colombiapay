package com.colombiapay.model.payables;

import java.util.Calendar;

public class ElectronicService extends Service {

    public ElectronicService(Double amount, String paymentReference, Calendar dueDate, String name) {
        super(amount, paymentReference, dueDate, name);
    }
}
