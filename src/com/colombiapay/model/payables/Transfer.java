package com.colombiapay.model.payables;

import java.util.Calendar;

public class Transfer extends Payable {

    private String destinationAccount;

    public Transfer(double amount, String paymentReference, Calendar dueDate, String destinationAccount) {
        super(amount, paymentReference, dueDate);
        this.destinationAccount = destinationAccount;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }
}
