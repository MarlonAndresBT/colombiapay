package com.colombiapay.model.payables;

import java.util.Calendar;

public abstract class Payable {
    public Double amount;
    public String paymentReference;
    public Calendar dueDate;

    public Payable(double amount, String paymentReference, Calendar dueDate) {
        this.amount = amount;
        this.paymentReference = paymentReference;
        this.dueDate = dueDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public Calendar getDueDate() {
        return dueDate;
    }

    public void setDueDate(Calendar dueDate) {
        this.dueDate = dueDate;
    }
}
