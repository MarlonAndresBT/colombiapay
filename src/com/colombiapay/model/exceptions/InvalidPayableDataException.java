package com.colombiapay.model.exceptions;

public class InvalidPayableDataException extends Exception {
    public InvalidPayableDataException() {
        super("The data entered does not correspond to any existing payable.");
    }

    public InvalidPayableDataException(String data) {
        super("The data: " + data + " does not correspond to any existing payable.");
    }
}
