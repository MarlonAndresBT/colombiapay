package com.colombiapay.model.exceptions;

public class cannotBeRemovedException extends Exception {
    public cannotBeRemovedException() {
        super("Element cannot be removed");
    }

    public cannotBeRemovedException(String message) {
        super("Element cannot be removed: " + message);
    }
}
