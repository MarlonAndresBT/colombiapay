package com.colombiapay.model.exceptions;

public class InvalidPaymentMethodDataException extends Exception {
    public InvalidPaymentMethodDataException() {
        super("The data entered does not correspond to any existing payment method.");
    }

    public InvalidPaymentMethodDataException(String data) {
        super("The data: " + data + " does not correspond to any existing payment method.");
    }
}
