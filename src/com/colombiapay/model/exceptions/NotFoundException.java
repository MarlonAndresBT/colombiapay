package com.colombiapay.model.exceptions;

public class NotFoundException extends Exception {
    public NotFoundException() {
        super("Item was not found");
    }

    public NotFoundException(String itemNotFound) {
        super("Item " + itemNotFound + " was not found");
    }
}
