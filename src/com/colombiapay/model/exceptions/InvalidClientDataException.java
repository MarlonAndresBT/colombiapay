package com.colombiapay.model.exceptions;

public class InvalidClientDataException extends Exception {
    public InvalidClientDataException() {
        super("The data inserted is not valid for the creation of a user");
    }

    public InvalidClientDataException(String data) {
        super("The data: " + data + " is not valid for the creation of a user");
    }


}
