package com.colombiapay.model.exceptions;

public class NotEnoughMoneyException extends Exception {
    public NotEnoughMoneyException() {
        super("Not enough money for this operation");
    }

    public NotEnoughMoneyException(String reason) {
        super("Not enough money for: " + reason);
    }
}
