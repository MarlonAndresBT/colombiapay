package com.colombiapay.model.exceptions;

public class UserNotFoundException extends Exception {
    public UserNotFoundException() {
        super("User not found");
    }

    public UserNotFoundException(String userName) {
        super("User " + userName + " not found");
    }
}
