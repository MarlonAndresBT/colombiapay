package com.colombiapay.ui;

import com.colombiapay.ui.styles.ColombiaPanel;

public class ColombiaPayPasswordRecovery extends ColombiaPanel {

    private static ColombiaPayPasswordRecovery colombiaPayPasswordRecovery;

    private ColombiaPayPasswordRecovery() {
        super();
    }

    public static synchronized ColombiaPanel getInstance() {
        if (colombiaPayPasswordRecovery == null)
            colombiaPayPasswordRecovery = new ColombiaPayPasswordRecovery();
        return colombiaPayPasswordRecovery;
    }
}
