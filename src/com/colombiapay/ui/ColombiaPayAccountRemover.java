package com.colombiapay.ui;

import com.colombiapay.App;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.ui.styles.ColombiaButton;
import com.colombiapay.ui.styles.ColombiaColor;
import com.colombiapay.ui.styles.ColombiaFont;
import com.colombiapay.ui.styles.ColombiaPanel;
import com.colombiapay.view.ColombiaPay;

import javax.swing.*;
import java.awt.*;

public class ColombiaPayAccountRemover extends ColombiaPanel {
    private static ColombiaPayAccountRemover colombiaPayAccountRemover;
    private Client client;

    private ColombiaPayAccountRemover(Client client) {
        initComponents(client);
    }

    public void initComponents(Client client) {
        this.removeAll();
        this.repaint();
        this.client = client;
        super.initComponents();
        initSuperTitle();
        initYesButton();
        initNoButton();
    }

    private void initYesButton() {
        JButton yesButton = new ColombiaButton("Si");
        yesButton.setLocation(425, 865);
        yesButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        yesButton.addActionListener(e -> {
            ColombiaPay.getInstance().getClients().remove(client.getCC());
            App.getInstance().setContentPane(ColombiaPayLogin.getInstance());
        });
        this.add(yesButton);
    }

    private void initNoButton() {
        JButton noButton = new ColombiaButton("No");
        noButton.setLocation(838, 865);
        noButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        noButton.addActionListener(e -> App.getInstance().goBack());
        this.add(noButton);
    }

    private void initSuperTitle() {
        JTextArea superTitle = new JTextArea("¿Segur@ que\ndeseas irte?");
        superTitle.setOpaque(false);
        superTitle.setEditable(false);
        superTitle.setSize(1026, 460);
        superTitle.setLocation(220, 250);
        superTitle.setFont(ColombiaFont.SUPER_TITLE);
        superTitle.setForeground(ColombiaColor.RED);
        this.add(superTitle);
    }

    public static synchronized ColombiaPayAccountRemover getInstance(Client client) {
        if (colombiaPayAccountRemover == null)
            colombiaPayAccountRemover = new ColombiaPayAccountRemover(client);
        colombiaPayAccountRemover.initComponents(client);
        return colombiaPayAccountRemover;
    }
}
