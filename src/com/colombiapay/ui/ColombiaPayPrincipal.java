package com.colombiapay.ui;

import com.colombiapay.App;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.ui.styles.ColombiaBigButton;
import com.colombiapay.ui.styles.ColombiaOptionButton;
import com.colombiapay.ui.styles.ColombiaPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ColombiaPayPrincipal extends ColombiaPanel {

    private static ColombiaPayPrincipal colombiaPayPrincipal;
    private JButton userButton;
    private JButton cashButton;
    private JButton exitButton;
    private JButton payButton;
    private JButton managePaymentMethodsButton;
    private JButton scheduleButton;
    private JButton invoiceButton;
    private Client client;


    private ColombiaPayPrincipal(Client client) {
        initComponents(client);
    }

    public void initComponents(Client client){
        this.removeAll();
        this.repaint();
        this.client = client;
        super.initComponents();
        initUserButton(client);
        initCashButton(client);
        initExitButton();
        initPayButton();
        initManagePaymentMethodsButton();
        initScheduleButton();
        initInvoiceButton();
    }

    private void initInvoiceButton() {
        invoiceButton = new ColombiaOptionButton("Facturas", "res/logo/Smashicons/400x400/cashier-1.png");
        invoiceButton.setLocation(989, 580);
        invoiceButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        //cashButton.addActionListener(this::validateRegistration);
        this.add(invoiceButton);
    }

    private void initScheduleButton() {
        scheduleButton = new ColombiaOptionButton("Programar Pago", "res/logo/Smashicons/400x400/time-is-money.png");
        scheduleButton.setLocation(500, 580);
        scheduleButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        //cashButton.addActionListener(this::validateRegistration);
        this.add(scheduleButton);
    }

    private void initManagePaymentMethodsButton() {
        managePaymentMethodsButton = new ColombiaOptionButton("Gestionar Medios de Pago", "res/logo/Smashicons/400x400/credit-card-3.png");
        managePaymentMethodsButton.setLocation(989, 120);
        managePaymentMethodsButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        //cashButton.addActionListener(this::validateRegistration);
        this.add(managePaymentMethodsButton);
    }

    private void initPayButton() {
        payButton = new ColombiaOptionButton("Pagar", "res/logo/Smashicons/400x400/atm-1.png");
        payButton.setLocation(500, 120);
        payButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        //cashButton.addActionListener(this::validateRegistration);
        this.add(payButton);
    }

    private void initExitButton() {
        exitButton = new ColombiaBigButton("Salir");
        exitButton.setLocation(90, 865);
        exitButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        exitButton.addActionListener(this::logOut);
        this.add(exitButton);
    }

    private void logOut(ActionEvent actionEvent) {
        App.getInstance().setPanel(this, ColombiaPayLogin.getInstance());
    }

    private void initCashButton(Client client) {
        cashButton = new ColombiaBigButton(client.amountMoney().toString());
        cashButton.setLocation(90, 310);
        cashButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cashButton.addActionListener(this::cashInfo);
        this.add(cashButton);
    }

    private void cashInfo(ActionEvent actionEvent) {
    }

    private void initUserButton(Client client) {
        userButton = new ColombiaBigButton(client.getName().split(" ")[0]);
        userButton.setLocation(90, 135);
        userButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        userButton.addActionListener(this::userInfo);
        this.add(userButton);
    }

    private void userInfo(ActionEvent actionEvent) {
        App.getInstance().setPanel(this, ColombiaPayUserInfo.getInstance(client));
    }


    public static synchronized ColombiaPayPrincipal getInstance(Client client) {
        if (colombiaPayPrincipal == null)
            colombiaPayPrincipal = new ColombiaPayPrincipal(client);
        colombiaPayPrincipal.initComponents(client);
        return colombiaPayPrincipal;
    }
}
