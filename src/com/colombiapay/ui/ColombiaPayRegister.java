package com.colombiapay.ui;

import com.colombiapay.App;
import com.colombiapay.model.exceptions.InvalidClientDataException;
import com.colombiapay.model.factories.ClientFactory;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.ui.styles.*;
import com.colombiapay.view.ColombiaPay;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ColombiaPayRegister extends ColombiaPanel {

    private static ColombiaPayRegister colombiaPayRegister;
    private JLabel superTitle;
    private JTextField identification;
    private JTextField fullName;
    private JPasswordField password;
    private JPasswordField confirmPassword;
    private JTextField cellPhone;
    private JTextField email;
    private JTextField birthdate;
    private JLabel invalidData;
    private JButton registerButton;
    private JLabel back;

    private ColombiaPayRegister() {
        super();
        initSuperTitle();
        initIdentification();
        initFullName();
        initPassword();
        initConfirmPassword();
        initCellPhone();
        initEmail();
        initBirthdate();
        initRegisterButton();
        initInvalidData();
        initBack();
    }

    private void initBack() {
        back = new JLabel("Atrás");
        back.setFont(ColombiaFont.HYPERLINK);
        back.setForeground(ColombiaColor.HYPERLINK);
        back.setBounds(721, 980, 59, 30);
        back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        back.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                App.getInstance().goBack();
            }
        });
        this.add(back);
    }

    private void initRegisterButton() {
        registerButton = new ColombiaButton("Registrarse");
        registerButton.setLocation(644, 910);
        registerButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        registerButton.addActionListener(this::validateRegistration);
        this.add(registerButton);
    }

    private void validateRegistration(ActionEvent actionEvent) {
        boolean userAlreadyExists = ColombiaPay.getInstance().getClients().containsKey(Long.parseLong(identification.getText()));
        if (userAlreadyExists) {
            String msg = " El Usuario " + identification.getText() + " ya existe, por favor inicie sesión";
            JOptionPane.showMessageDialog(App.getInstance(), msg, " ", JOptionPane.ERROR_MESSAGE);
            App.getInstance().goBack();
        } else {
            StringBuilder dataClient = new StringBuilder();
            dataClient.append(identification.getText()).append(" ");
            dataClient.append(fullName.getText()).append(" ");
            if (birthdate.getText().length() == 10 && password.getText().equals(confirmPassword.getText())) {
                String[] date = birthdate.getText().split("/");
                String day = date[0];
                String month = date[1];
                String year = date[2];
                dataClient.append(day).append(" ").append(month).append(" ").append(year).append(" ");
                dataClient.append(email.getText()).append(" ");
                dataClient.append(cellPhone.getText()).append(" ");
                dataClient.append(password.getText());
                try {
                    Client newClient = ClientFactory.build(dataClient.toString());
                    ColombiaPay.getInstance().addClient(newClient);
                    App.getInstance().getBackNavigator().push(this);
                    App.getInstance().setContentPane(ColombiaPayWelcome.getInstance(newClient));
                } catch (InvalidClientDataException e) {
                    invalidData.setVisible(true);
                }
            } else
                invalidData.setVisible(true);
        }
    }

    private void initInvalidData() {
        invalidData = new JLabel("Datos invalidos, intente nuevamente");
        invalidData.setVisible(false);
        invalidData.setBounds(600, 863, 346, 40);
        invalidData.setAlignmentX(registerButton.getAlignmentX());
        invalidData.setFont(ColombiaFont.LOGIN_ERROR_MESSAGE);
        invalidData.setForeground(ColombiaColor.LOGIN_ERROR_MESSAGE);
        invalidData.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        this.add(invalidData);
    }

    private void initBirthdate() {
        birthdate = new RoundedJTextField(15);
        birthdate.setFont(ColombiaFont.TEXT_FIELD);
        birthdate.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 208, posY = 771;
        birthdate.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Fecha de Nacimiento DD/MM/AAAA");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        birthdate.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = birthdate.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                invalidData.setVisible(false);
            }
        });
        birthdate.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerButton.doClick();
            }

        });
        birthdate.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {

                boolean fieldIsEmpty = birthdate.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                char keyChar = ke.getKeyChar();
                boolean isNumber = Character.toString(keyChar).matches("[0-9]"),
                        isDelete = Character.toString(keyChar).matches("[\b]");
                String date = birthdate.getText();
                int dateLength = date.length();
                if (isDelete)
                    birthdate.setEditable(true);
                else if (dateLength == 2 || dateLength == 5)
                    birthdate.setText(date + "/");
                else
                    birthdate.setEditable(isNumber && dateLength < 10);
            }
        });
        this.add(textOnField);
        this.add(birthdate);
    }

    private void initEmail() {
        email = new RoundedJTextField(15);
        email.setFont(ColombiaFont.TEXT_FIELD);
        email.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 758, posY = 697;
        email.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Correo");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        email.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = email.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                invalidData.setVisible(false);
            }
        });
        email.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerButton.doClick();
            }

        });
        email.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {
                boolean fieldIsEmpty = email.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                char keyChar = ke.getKeyChar();
                boolean isLetter = Character.toString(keyChar).matches("\\w"),
                        isSpace = Character.toString(keyChar).matches("\\s"),
                        isDelete = Character.toString(keyChar).matches("[\b]"),
                        isDot = Character.toString(keyChar).matches("[.]"),
                        isAt = Character.toString(keyChar).matches("[@]");
                email.setEditable(isLetter || isDelete || isSpace || isDot || isAt);
            }
        });

        this.add(textOnField);
        this.add(email);
    }

    private void initCellPhone() {
        cellPhone = new RoundedJTextField(15);
        cellPhone.setFont(ColombiaFont.TEXT_FIELD);
        cellPhone.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 758, posY = 626;
        cellPhone.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Numero de Celular");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        cellPhone.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = cellPhone.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                invalidData.setVisible(false);
            }
        });
        cellPhone.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerButton.doClick();
            }

        });
        cellPhone.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {

                boolean fieldIsEmpty = cellPhone.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                char keyChar = ke.getKeyChar();
                boolean isNumber = Character.toString(keyChar).matches("[0-9]"),
                        isDelete = Character.toString(keyChar).matches("[\b]");
                cellPhone.setEditable(isNumber || isDelete);
            }
        });

        this.add(textOnField);
        this.add(cellPhone);
    }

    private void initConfirmPassword() {
        confirmPassword = new RoundedJPasswordField(15);
        confirmPassword.setFont(ColombiaFont.TEXT_FIELD);
        confirmPassword.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 208, posY = 697;
        confirmPassword.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Confirmar contraseña");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        confirmPassword.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = confirmPassword.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                invalidData.setVisible(false);
            }
        });

        confirmPassword.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {

                boolean fieldIsEmpty = confirmPassword.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
            }
        });

        this.add(textOnField);
        this.add(confirmPassword);
    }

    private void initPassword() {
        password = new RoundedJPasswordField(15);
        password.setFont(ColombiaFont.TEXT_FIELD);
        password.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 208, posY = 626;
        password.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Contraseña");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        password.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = password.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                invalidData.setVisible(false);
            }
        });

        password.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {

                boolean fieldIsEmpty = password.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                /*invalidData.setText("Contraseña invalida, debe contener Letras mayusculas, minusculas, numeros, simbolos");
                invalidData.setLocation(registerButton.getLocation().x,invalidData.getLocation().y);
                boolean condition = !password.getText().matches("^(?=.{8,20}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).*$");
                invalidData.setVisible(condition);*/
            }
        });

        this.add(textOnField);
        this.add(password);
    }

    private void initFullName() {
        fullName = new RoundedJTextField(15);
        fullName.setFont(ColombiaFont.TEXT_FIELD);
        fullName.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 758, posY = 552;
        fullName.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Nombre Completo");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        fullName.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = identification.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                invalidData.setVisible(false);
            }
        });
        fullName.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerButton.doClick();
            }

        });
        fullName.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {
                boolean fieldIsEmpty = fullName.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                char keyChar = ke.getKeyChar();
                boolean isLetter = Character.toString(keyChar).matches("[a-zA-ZñÑ]"),
                        isSpace = Character.toString(keyChar).matches("\\s"),
                        isDelete = Character.toString(keyChar).matches("[\b]");
                fullName.setEditable(isLetter || isDelete || isSpace);
            }
        });

        this.add(textOnField);
        this.add(fullName);
    }

    private void initIdentification() {
        identification = new RoundedJTextField(15);
        identification.setFont(ColombiaFont.TEXT_FIELD);
        identification.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 208, posY = 552;
        identification.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Cédula");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        identification.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = identification.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                invalidData.setVisible(false);
            }
        });
        identification.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerButton.doClick();
            }

        });
        identification.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {

                boolean fieldIsEmpty = identification.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                char keyChar = ke.getKeyChar();
                boolean isNumber = Character.toString(keyChar).matches("[0-9]"),
                        isDelete = Character.toString(keyChar).matches("[\b]");
                identification.setEditable(isNumber || isDelete);
            }
        });

        this.add(textOnField);
        this.add(identification);
    }

    public void initSuperTitle() {
        superTitle = new JLabel("ColombiaPay");
        superTitle.setSize(1051, 230);
        superTitle.setLocation(189, 244);
        superTitle.setFont(ColombiaFont.SUPER_TITLE);
        superTitle.setForeground(ColombiaColor.RED);
        this.add(superTitle);
    }

    public static synchronized ColombiaPanel getInstance() {
        if (colombiaPayRegister == null)
            colombiaPayRegister = new ColombiaPayRegister();
        return colombiaPayRegister;
    }
}
