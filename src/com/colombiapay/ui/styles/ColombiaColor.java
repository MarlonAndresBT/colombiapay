package com.colombiapay.ui.styles;

import java.awt.*;

public interface ColombiaColor {
    Color RED = Color.decode("#F2746D");
    Color BLUE = Color.decode("#55A5F2");
    Color WHITE = Color.decode("#F2F2F7");
    Color HYPERLINK = Color.decode("#0A84FF");
    Color LABEL_TEXT_FIELD = new Color(65, 91, 112, 100);
    Color LOGIN_ERROR_MESSAGE = new Color(241, 16, 2);
    Color BOX_TEXT_FIELD = new Color(RED.getRed(), RED.getGreen(), RED.getBlue(), 4);
}