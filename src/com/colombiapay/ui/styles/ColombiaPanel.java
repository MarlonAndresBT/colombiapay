package com.colombiapay.ui.styles;

import javax.swing.*;

import static com.colombiapay.App.WINDOW_SIZE;

public abstract class ColombiaPanel extends JPanel {

    public String args;

    public ColombiaPanel() {
        initComponents();
    }

    public ColombiaPanel(String args) {
        this.args = args;
        JPanel topBar = topBar();
        this.panelConfigurator();
        this.add(topBar);
    }

    public void initComponents(){
        JPanel topBar = topBar();
        this.panelConfigurator();
        this.add(topBar);
    }

    private JPanel topBar() {
        JPanel topBar = new JPanel();
        topBar.setBounds(0, 0, WINDOW_SIZE.width, 50);
        topBar.setBackground(ColombiaColor.RED);
        return topBar;
    }

    private void panelConfigurator() {
        this.setLayout(null);
        this.setVisible(true);
        this.setSize(WINDOW_SIZE);
        this.setBackground(ColombiaColor.WHITE);
    }

}
