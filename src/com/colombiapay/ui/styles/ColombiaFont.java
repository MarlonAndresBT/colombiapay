package com.colombiapay.ui.styles;

import java.awt.*;

public interface ColombiaFont {
    Font BUTTON = new Font("Geeza Pro", Font.PLAIN, 30);
    Font BIG_BUTTON = new Font("Silom", Font.PLAIN, 40);
    Font HYPERLINK = new Font("Geeza Pro", Font.PLAIN, 20);
    Font TEXT_FIELD = new Font("Helvetica", Font.PLAIN, 30);
    Font TEXT_FIELD_USER_INFO = new Font("Helvetica", Font.BOLD, 36);
    Font SUPER_TITLE = new Font("Silom", Font.PLAIN, 150);
    Font LABEL_TEXT_FIELD = new Font("Geeza Pro", Font.PLAIN, 30);
    Font LOGIN_ERROR_MESSAGE = new Font("Helvetica", Font.PLAIN, 20);
}
