package com.colombiapay.ui.styles;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

public class RoundedJPasswordField extends JPasswordField {
    final int ARCO = 32;
    private Shape shape;

    public RoundedJPasswordField(int size) {
        super(size);
        setOpaque(false);
    }

    protected void paintComponent(Graphics g) {
        g.setColor(getBackground());
        g.fillRoundRect(0, 0, getWidth() - 1, getHeight(), ARCO, ARCO);
        super.paintComponent(g);
    }

    protected void paintBorder(Graphics g) {
        g.setColor(ColombiaColor.RED);
        g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, ARCO, ARCO);
    }

    public boolean contains(int x, int y) {
        if (shape == null || !shape.getBounds().equals(getBounds())) {
            shape = new RoundRectangle2D.Double(0, 0, getWidth() - 1, getHeight() - 1, ARCO, ARCO);
        }
        return shape.contains(x, y);
    }
}