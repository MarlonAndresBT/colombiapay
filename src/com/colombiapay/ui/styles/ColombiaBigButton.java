package com.colombiapay.ui.styles;

import javax.swing.*;

public class ColombiaBigButton extends JButton {
    public ColombiaBigButton(String text) {
        super(text);
        this.setFont(ColombiaFont.BIG_BUTTON);
        this.setSize(400, 126);
        this.setBorder(new RoundedBorder(50));
        this.setForeground(ColombiaColor.WHITE);
        Icon iconButton = new ImageIcon("res/ui/Button/BigRectangle.png");
        this.setIcon(iconButton);
        this.setVerticalTextPosition(SwingConstants.CENTER);
        this.setHorizontalTextPosition(SwingConstants.CENTER);
    }
}
