package com.colombiapay.ui.styles;

import javax.swing.*;

public class ColombiaOptionButton extends JButton {
    public ColombiaOptionButton(String text, String icon) {
        super(text);
        this.setFont(ColombiaFont.BIG_BUTTON);
        this.setSize(400, 400);
        this.setBorder(new RoundedBorder(25));
        this.setForeground(ColombiaColor.RED);
        Icon iconButton = new ImageIcon(icon);
        this.setIcon(iconButton);
        this.setVerticalTextPosition(SwingConstants.TOP);
        this.setHorizontalTextPosition(SwingConstants.CENTER);
    }
}
