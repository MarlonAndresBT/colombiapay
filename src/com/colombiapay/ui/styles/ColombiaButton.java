package com.colombiapay.ui.styles;

import javax.swing.*;

public class ColombiaButton extends JButton {
    public ColombiaButton(String text) {
        super(text);
        this.setFont(ColombiaFont.BUTTON);
        this.setSize(208, 50);
        this.setBorder(new RoundedBorder(25));
        this.setForeground(ColombiaColor.WHITE);
        Icon iconButton = new ImageIcon("res/ui/Button/Rectangle.png");
        this.setIcon(iconButton);
        this.setVerticalTextPosition(SwingConstants.CENTER);
        this.setHorizontalTextPosition(SwingConstants.CENTER);
    }
}
