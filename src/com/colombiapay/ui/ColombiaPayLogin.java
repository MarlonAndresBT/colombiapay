package com.colombiapay.ui;

import com.colombiapay.App;
import com.colombiapay.model.ErrorLog;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.ui.styles.*;
import com.colombiapay.view.ColombiaPay;

import javax.security.sasl.AuthenticationException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ColombiaPayLogin extends ColombiaPanel {

    private static ColombiaPayLogin colombiaPayLogin;
    private JLabel superTitle;
    private JTextField identification;
    private JPasswordField password;
    private JLabel forgotPassword;
    private JLabel userOrPasswordInvalid;
    private JButton registerButton;
    private JButton loginButton;

    private ColombiaPayLogin() {
        super();
        initComponents();
    }

    public void initComponents() {
        this.removeAll();
        this.repaint();
        super.initComponents();
        initSuperTitle();
        initIdentification();
        initPassword();
        initForgotPassword();
        initUserOrPasswordInvalid();
        initLoginButton();
        initRegisterButton();
    }

    public static synchronized ColombiaPayLogin getInstance() {
        if (colombiaPayLogin == null)
            colombiaPayLogin = new ColombiaPayLogin();
        return colombiaPayLogin;
    }

    public void initForgotPassword() {
        forgotPassword = new JLabel("¿Olvidaste tu contraseña?");
        forgotPassword.setForeground(ColombiaColor.HYPERLINK);
        forgotPassword.setFont(ColombiaFont.HYPERLINK);
        forgotPassword.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        forgotPassword.setLocation(473, 733);
        forgotPassword.setSize(235, 26);
        forgotPassword.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                recoverPasswordScreen();
            }
        });
        this.add(forgotPassword);
    }

    public void initRegisterButton() {
        registerButton = new ColombiaButton("Registrarse");
        registerButton.setLocation(742, 833);
        registerButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        registerButton.addActionListener(this::registrationScreen);
        this.add(registerButton);
    }

    public void initLoginButton() {
        loginButton = new ColombiaButton("Ingresar");
        loginButton.setLocation(497, 833);
        loginButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        loginButton.addActionListener(this::checkLogin);
        this.add(loginButton);
    }

    public void initSuperTitle() {
        superTitle = new JLabel("ColombiaPay");
        superTitle.setSize(1051, 230);
        superTitle.setLocation(189, 244);
        superTitle.setFont(ColombiaFont.SUPER_TITLE);
        superTitle.setForeground(ColombiaColor.RED);
        this.add(superTitle);
    }

    public void initIdentification() {
        identification = new RoundedJTextField(15);
        identification.setFont(ColombiaFont.TEXT_FIELD);
        identification.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 473, posY = 560;
        identification.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Cédula");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        identification.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = identification.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                userOrPasswordInvalid.setVisible(false);
            }
        });
        identification.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loginButton.doClick();
            }

        });
        identification.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {

                boolean fieldIsEmpty = identification.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                char keyChar = ke.getKeyChar();
                boolean isNumber = Character.toString(keyChar).matches("[0-9]");
                boolean isDelete = Character.toString(keyChar).matches("[\b]");
                identification.setEditable(isNumber || isDelete);
            }
        });
        this.add(textOnField);
        this.add(identification);
    }

    public void initPassword() {
        password = new RoundedJPasswordField(15);
        password.setFont(ColombiaFont.TEXT_FIELD);
        password.setBackground(ColombiaColor.BOX_TEXT_FIELD);
        int posX = 473, posY = 654;
        password.setBounds(posX, posY, 501, 49);

        JLabel textOnField = new JLabel("Contraseña");
        textOnField.setFont(ColombiaFont.LABEL_TEXT_FIELD);
        textOnField.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        textOnField.setBounds(posX + 4, posY + 2, 501, 49);

        password.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean fieldIsEmpty = identification.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);
                userOrPasswordInvalid.setVisible(false);
            }
        });

        password.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {

                boolean fieldIsEmpty = identification.getText().isEmpty();
                textOnField.setVisible(fieldIsEmpty);

            }
        });

        password.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loginButton.doClick();
            }
        });
        this.add(textOnField);
        this.add(password);
    }

    public boolean userExists(Long cc) {
        ColombiaPay app = ColombiaPay.getInstance();
        return app.getClients().containsKey(cc);
    }

    public void checkPassword(Long cc, String password) throws AuthenticationException {
        ColombiaPay app = ColombiaPay.getInstance();
        app.getClients().getValue(cc).checkPassword(password);
    }

    private void checkLogin(ActionEvent e) {
        String idFieldString = identification.getText();
        try {
            Long userCC = Long.parseLong(idFieldString);
            if (userExists(userCC)) {
                String userPassword = password.getText();
                checkPassword(userCC, userPassword);
                login(ColombiaPay.getInstance().getClients().getValue(userCC));
            } else
                userOrPasswordInvalid.setVisible(true);
        } catch (Exception exception) {
            ErrorLog.report(exception);
            userOrPasswordInvalid.setVisible(true);
        }
    }

    private void initUserOrPasswordInvalid() {
        userOrPasswordInvalid = new JLabel("Usuario o Contraseña no Validos");
        userOrPasswordInvalid.setVisible(false);
        userOrPasswordInvalid.setBounds(550, 785, 346, 40);
        userOrPasswordInvalid.setFont(ColombiaFont.LOGIN_ERROR_MESSAGE);
        userOrPasswordInvalid.setForeground(ColombiaColor.LOGIN_ERROR_MESSAGE);
        userOrPasswordInvalid.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        this.add(userOrPasswordInvalid);
    }

    private void login(Client client) {
        System.out.println("██        \n" +
                "██        \n" +
                "██        \n" +
                "██        \n" +
                "███████   \n" +
                "          \n" +
                "          \n" +
                " ██████   \n" +
                "██    ██  \n" +
                "██    ██  \n" +
                "██    ██  \n" +
                " ██████   \n" +
                "          \n" +
                "          \n" +
                " ██████   \n" +
                "██        \n" +
                "██   ███  \n" +
                "██    ██  \n" +
                " ██████   \n" +
                "          \n" +
                "          \n" +
                "██        \n" +
                "██        \n" +
                "██        \n" +
                "██        \n" +
                "██        \n" +
                "          \n" +
                "          \n" +
                "███    ██ \n" +
                "████   ██ \n" +
                "██ ██  ██ \n" +
                "██  ██ ██ \n" +
                "██   ████ \n" +
                "          ");
        App.getInstance().setPanel(this, ColombiaPayWelcome.getInstance(client));
    }

    private void recoverPasswordScreen() {
        System.out.println("Recover password screen");
    }

    private void registrationScreen(ActionEvent actionEvent) {
        App window = App.getInstance();
        window.getBackNavigator().push(this);
        ColombiaPanel registerPanel = ColombiaPayRegister.getInstance();
        window.setContentPane(registerPanel);
    }

}
