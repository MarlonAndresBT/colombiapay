package com.colombiapay.ui;

import com.colombiapay.App;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.ui.styles.ColombiaButton;
import com.colombiapay.ui.styles.ColombiaColor;
import com.colombiapay.ui.styles.ColombiaFont;
import com.colombiapay.ui.styles.ColombiaPanel;
import com.colombiapay.view.ColombiaPay;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ColombiaPayWelcome extends ColombiaPanel {
    private static ColombiaPayWelcome colombiaPayWelcome;
    private JButton startButton;
    private Client client;

    private ColombiaPayWelcome(Client client) {
        super();
        initComponents(client);
    }

    public void initComponents(Client client) {
        this.removeAll();
        this.repaint();
        this.client = client;
        super.initComponents();
        initSuperTitle(client);
        initBack();
        initStartButton();
    }

    public void initSuperTitle(Client client) {
        String userName = client.getName().split(" ")[0];
        JTextArea superTitle = new JTextArea("¡Bienvenid@\n" + userName + "!");
        superTitle.setOpaque(false);
        superTitle.setEditable(false);
        superTitle.setSize(1026, 460);
        superTitle.setLocation(220, 250);
        superTitle.setFont(ColombiaFont.SUPER_TITLE);
        superTitle.setForeground(ColombiaColor.RED);
        this.add(superTitle);
    }

    private void initBack() {
        JLabel back = new JLabel("Atrás");
        back.setFont(ColombiaFont.HYPERLINK);
        back.setForeground(ColombiaColor.HYPERLINK);
        back.setBounds(721, 980, 59, 30);
        back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        back.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                App.getInstance().goBack();
            }
        });
        this.add(back);
    }

    private void initStartButton() {
        startButton = new ColombiaButton("Iniciar");
        startButton.setLocation(644, 910);
        startButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        startButton.addActionListener(this::startApp);
        this.add(startButton);
    }

    private void startApp(ActionEvent actionEvent) {
        App.getInstance().getBackNavigator().push(this);
        App.getInstance().setContentPane(ColombiaPayPrincipal.getInstance(client));
    }

    public static synchronized ColombiaPayWelcome getInstance(Client client) {
        if (colombiaPayWelcome == null)
            colombiaPayWelcome = new ColombiaPayWelcome(client);
        colombiaPayWelcome.initComponents(client);
        return colombiaPayWelcome;
    }
}
