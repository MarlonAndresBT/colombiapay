package com.colombiapay.ui;

import com.colombiapay.App;
import com.colombiapay.model.user_data.Client;
import com.colombiapay.ui.styles.*;
import com.colombiapay.view.ColombiaPay;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Calendar;

public class ColombiaPayUserInfo extends ColombiaPanel {

    private static ColombiaPayUserInfo colombiaPayUserInfo;
    private Client client;
    private JButton deleteButton;
    private JButton cashButton;
    private JButton userButton;
    private JButton backButton;
    private JLabel name;
    private JLabel cc;
    private JLabel birthdate;
    private JLabel cel;
    private JLabel mail;

    private ColombiaPayUserInfo(Client client) {
        initComponents(client);
    }

    public void initComponents(Client client){
        this.removeAll();
        this.repaint();
        this.client = client;
        super.initComponents();
        initUserButton(client);
        initCashButton(client);
        initDeleteButton();
        initBackButton();
        initLabels(client);
    }

    private void initLabels(Client client) {
        name = new JLabel(client.getName());
        name.setFont(ColombiaFont.TEXT_FIELD_USER_INFO);
        name.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        name.setBounds(644,177,586,47);

        cc = new JLabel(client.getCC().toString());
        cc.setFont(ColombiaFont.TEXT_FIELD_USER_INFO);
        cc.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        cc.setBounds(644,289,586,47);

        birthdate = new JLabel(client.getBirthDate().get(Calendar.DAY_OF_MONTH) + "/"
                + client.getBirthDate().get(Calendar.MONTH)+1 + "/"
                + client.getBirthDate().get(Calendar.YEAR));
        birthdate.setFont(ColombiaFont.TEXT_FIELD_USER_INFO);
        birthdate.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        birthdate.setBounds(644,405,586,47);

        cel = new JLabel(client.getCellPhone().toString());
        cel.setFont(ColombiaFont.TEXT_FIELD_USER_INFO);
        cel.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        cel.setBounds(644,526,586,47);

        mail = new JLabel(client.getEmail());
        mail.setFont(ColombiaFont.TEXT_FIELD_USER_INFO);
        mail.setForeground(ColombiaColor.LABEL_TEXT_FIELD);
        mail.setBounds(644,647,586,47);

        this.add(name);
        this.add(cc);
        this.add(birthdate);
        this.add(cel);
        this.add(mail);
    }

    private void initBackButton(){
        deleteButton = new ColombiaButton("Atras");
        deleteButton.setLocation(644,910);
        deleteButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        deleteButton.addActionListener(this::goBack);
        this.add(deleteButton);
    }

    private void initDeleteButton() {
        deleteButton = new ColombiaBigButton("Eliminar");
        deleteButton.setLocation(90,485);
        deleteButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        deleteButton.addActionListener(e -> App.getInstance().setPanel(this, ColombiaPayAccountRemover.getInstance(client)));
        this.add(deleteButton);
    }

    private void goBack(ActionEvent actionEvent) {
        App.getInstance().goBack();
    }

    private void initCashButton(Client client) {
        cashButton = new ColombiaBigButton(client.amountMoney().toString());
        cashButton.setLocation(90,310);
        cashButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cashButton.addActionListener(this::cashInfo);
        this.add(cashButton);
    }

    private void cashInfo(ActionEvent actionEvent) {
    }

    private void initUserButton(Client client) {
        userButton = new ColombiaBigButton(client.getName().split(" ")[0]);
        userButton.setLocation(90,135);
        userButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        userButton.addActionListener(this::userInfo);
        this.add(userButton);
    }

    private void userInfo(ActionEvent actionEvent) {
        App.getInstance().setPanel(this,ColombiaPayUserInfo.getInstance(client));
    }

    public static synchronized ColombiaPanel getInstance(Client client) {
        if (colombiaPayUserInfo == null)
            colombiaPayUserInfo = new ColombiaPayUserInfo(client);
        colombiaPayUserInfo.initComponents(client);
        return colombiaPayUserInfo;
    }
}
