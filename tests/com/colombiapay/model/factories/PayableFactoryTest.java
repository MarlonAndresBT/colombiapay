package com.colombiapay.model.factories;

import com.colombiapay.model.payables.Credit;
import com.colombiapay.model.payables.Transfer;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

class PayableFactoryTest {
    @Test
    void dataValidation() throws Exception{
        Method method = PayableFactory.class.getDeclaredMethod("dataValidation", String.class);
        method.setAccessible(true);
        int returnValue = (int) method.invoke(PayableFactory.class, "Credit 10000.90 ASD87-A 20 04 2000 130000.00 1400000.00");
        assertEquals(returnValue, PayableFactory.CREDIT);

        returnValue = (int) method.invoke(PayableFactory.class, "Transfer 10000.90 ASD87A 20 04 2000 B19219280-3103129");
        assertEquals(returnValue, PayableFactory.TRANSFER);

        returnValue = (int) method.invoke(PayableFactory.class, "Agua 120500.25 ASD87-A 20 04 2000 Acueducto");
        assertEquals(returnValue, PayableFactory.SERVICE);

        returnValue = (int) method.invoke(PayableFactory.class, "Tax 10000 ASD87A 20 04 2000");
        assertEquals(returnValue, PayableFactory.PAYABLE);

        returnValue = (int) method.invoke(PayableFactory.class, "Tax 10000.1234 ASD87A 20 04 2000");
        assertEquals(returnValue, PayableFactory.PAYABLE);

        assertThrows(Exception.class, () -> method.invoke(PaymentMethodFactory.class, "agua 120500.25 ASD87-A 20 04 2000 Acueducto"));
        assertThrows(Exception.class, () -> method.invoke(PaymentMethodFactory.class, "Tax 10000 ASD87A 20 04"));
        assertThrows(Exception.class, () -> method.invoke(PaymentMethodFactory.class, "Tax 10000 20 04 2000"));
    }

    @Test
    void creditBuilder() throws NoSuchMethodException {
        Method method = PayableFactory.class.getDeclaredMethod("creditBuilder", String.class);
        method.setAccessible(true);
        String data = "Credit 10000.90 ASD87-A 20 04 2000 130000.00 1400000.00";
        try {
            Credit creditPayable = (Credit) method.invoke(PayableFactory.class, data);
            assertNotNull(creditPayable);
        }catch (Exception e){
            fail(e.getMessage());
        }
    }

    @Test
    void transferBuilder() throws NoSuchMethodException {
        Method method = PayableFactory.class.getDeclaredMethod("transferBuilder", String.class);
        method.setAccessible(true);
        String data = "Transfer 10000.90 ASD87A 20 04 2000 B19219280-3103129";
        try {
            Transfer transferPayable = (Transfer) method.invoke(PayableFactory.class, data);
            assertNotNull(transferPayable);
            assertEquals(data.split("\\s")[6],transferPayable.getDestinationAccount());
            assertEquals(data.split("\\s")[2],transferPayable.getPaymentReference());
            assertEquals(Double.parseDouble(data.split("\\s")[1]),transferPayable.getAmount());
        }catch (Exception e){
            fail(e.getMessage());
        }
    }

}