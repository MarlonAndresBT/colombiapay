package com.colombiapay.model.factories;

import com.colombiapay.model.exceptions.InvalidPaymentMethodDataException;
import com.colombiapay.model.payment_methods.PaymentMethod;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import static org.junit.jupiter.api.Assertions.*;

class PaymentMethodFactoryTest {

    @Test
    void build() {
        try {
            PaymentMethodFactory.build("CreditCard 4532 0840 3898 8286 2 2021 123 1000000.0 1500000.0 1660031056799");
            PaymentMethodFactory.build("DebitCard 4532 0840 3898 8286 11 2021 123 1000000.0 1660031056799");
            PaymentMethodFactory.build("ColombiaCredits 1129984787 1000000.0 1660031056799");
        } catch (InvalidPaymentMethodDataException e) {
            fail(e.getMessage());
        }
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("DebitCard 4532 0840 3898 8286 2 2021 123 1000000.0 1500000.0 1660031056799"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("CreditCard 4532 3898 8286 2 2021 123 1000000.0 1500000.0 1660031056799"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("CreditCard 4532 0840 3898 8286 123 1000000.0 1500000.0 1660031056799"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("CreditCard 4532 0840 3898 8286 2 2021 1000000.0 1500000.0 1660031056799"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("CreditCard 4532 0840 3898 8286 2 2021 123 1500000.0 1660031056799"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("CreditCard 4532 0840 3898 8286 2 2021 123 1000000.0 1500000.0"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("ColombiaCredits 4532 0840 3898 8286 2 2021 123 1000000.0 1500000.0 1660031056799"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("ColombiaCredits 1129984787 1000000.0"));
        assertThrows(Exception.class, () -> PaymentMethodFactory.build("ColombiaCredits 1129984787 1660031056799"));
    }

    @Test
    void dataValidation() throws Exception{
        Method method = PaymentMethodFactory.class.
                getDeclaredMethod("dataValidation", String.class);
        method.setAccessible(true);

        int returnValue = (int) method.invoke(PaymentMethodFactory.class,
                "CreditCard 4532 0840 3898 8286 2 2021 123 1000000.0 1500000.0 1660031056799");
        assertEquals(PaymentMethodFactory.CREDIT_CARD, returnValue);

        returnValue = (int) method.invoke(PaymentMethodFactory.class,
                "DebitCard 4532 0840 3898 8286 11 2021 123 1000000.0 1660031056799");
        assertEquals(PaymentMethodFactory.DEBIT_CARD, returnValue);

        returnValue = (int) method.invoke(PaymentMethodFactory.class,
                "ColombiaCredits 1129984787 1000000.0 1660031056799");
        assertEquals(PaymentMethodFactory.COLOMBIA_CREDITS, returnValue);

        assertThrows(Exception.class, () -> method.invoke(PaymentMethodFactory.class, "DebitCard 4532 0840 3898 8286 11 2021 123 1000000.0"));
        assertThrows(Exception.class, () -> method.invoke(PaymentMethodFactory.class, "CreditCard 4532 0840 3898 8286 2 2021 123 1000000.0 1660031056799"));
        assertThrows(Exception.class, () -> method.invoke(PaymentMethodFactory.class, "1129984787 1000000.0 1660031056799"));
    }

}