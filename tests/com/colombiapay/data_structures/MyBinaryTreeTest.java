package com.colombiapay.data_structures;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class MyBinaryTreeTest {
    MyBinaryTree<Integer, Character> myTree;

    void TestCase() {
        myTree = new MyBinaryTree<>();
        myTree.put(5, 'A');
        myTree.put(2, 'B');
        myTree.put(8, 'C');
        myTree.put(1, 'D');
        myTree.put(4, 'E');
        myTree.put(6, 'F');
        myTree.put(9, 'G');
        myTree.put(3, 'H');
        myTree.put(7, 'I');
    }

    @org.junit.jupiter.api.Test
    void put() {
        TestCase();
        assertFalse(myTree.isEmpty());
        assertNotEquals(null, myTree.getRoot());
    }

    @org.junit.jupiter.api.Test
    void containsKey() {
        TestCase();
        for (int i = 1; i < 10; i++) {
            assertTrue(myTree.containsKey(i));
        }
    }

    @org.junit.jupiter.api.Test
    void containsValue() {
        TestCase();
        for (char c = 'A'; c <= 'I'; c++) {
            assertTrue(myTree.containsValue(c));
        }
        for (char c = 'J'; c <= 'Z'; c++) {
            assertFalse(myTree.containsValue(c));
        }
    }

    @Test
    void remove() {
        TestCase();
        int size = myTree.size();
        for (int i = 1; i <= size; i++) {
            myTree.levelOrder();
            System.out.println("Size: " + myTree.size());
            myTree.remove(i);
            assertEquals(size-i,myTree.size());
        }
        System.out.println("Size: " + myTree.size());
        assertEquals(0,myTree.size());
        assertTrue(myTree.isEmpty());
    }

    @org.junit.jupiter.api.Test
    void get() {
        TestCase();
        assertEquals('A', myTree.getValue(5));
        assertEquals('B', myTree.getValue(2));
        assertEquals('C', myTree.getValue(8));
        assertEquals('D', myTree.getValue(1));
        assertEquals('E', myTree.getValue(4));
        assertEquals('F', myTree.getValue(6));
        assertEquals('G', myTree.getValue(9));
        assertEquals('H', myTree.getValue(3));
        assertEquals('I', myTree.getValue(7));
    }

    @org.junit.jupiter.api.Test
    void findMin() {
        TestCase();
        assertEquals('D', myTree.findMin());
    }

    @org.junit.jupiter.api.Test
    void findMax() {
        TestCase();
        assertEquals('G', myTree.findMax());
    }

    @org.junit.jupiter.api.Test
    void height() {
        TestCase();
        assertEquals(3, myTree.height());
    }

    @org.junit.jupiter.api.Test
    void preOrder() {
        TestCase();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        myTree.preOrder();
        String expectedOutput = "[A] [B] [D] [E] [H] [C] [F] [I] [G] ";
        assertEquals(expectedOutput, outContent.toString());
    }

    @org.junit.jupiter.api.Test
    void inOrder() {
        TestCase();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        myTree.inOrder();
        String expectedOutput = "[D] [B] [H] [E] [A] [F] [I] [C] [G] ";
        assertEquals(expectedOutput, outContent.toString());
    }

    @org.junit.jupiter.api.Test
    void postOrder() {
        TestCase();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        myTree.postOrder();
        String expectedOutput = "[D] [H] [E] [B] [I] [F] [G] [C] [A] ";
        assertEquals(expectedOutput, outContent.toString());
    }

    @org.junit.jupiter.api.Test
    void levelOrder() {
        TestCase();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        myTree.levelOrder();
        String expectedOutput = "[A] [B] [C] [D] [E] [F] [G] [H] [I] ";
        assertEquals(expectedOutput, outContent.toString());
    }

    @org.junit.jupiter.api.Test
    void size() {
        TestCase();
        assertEquals(9, myTree.size());
        myTree.remove(1);
        assertEquals(8, myTree.size());
        myTree.makeEmpty();
        assertEquals(0, myTree.size());
    }

    @org.junit.jupiter.api.Test
    void makeEmpty() {
        TestCase();
        assertFalse(myTree.isEmpty());
        myTree.makeEmpty();
        assertTrue(myTree.isEmpty());
    }

    @org.junit.jupiter.api.Test
    void isEmpty() {
        MyBinaryTree<Integer, Character> myTree = new MyBinaryTree<>();
        assertTrue(myTree.isEmpty());
        for (int i = 0; i < 10; i++) {
            myTree.put(i, 'A');
        }
        assertFalse(myTree.isEmpty());
    }
}